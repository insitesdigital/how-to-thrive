<?php
/* Template Name: Sitemap */
?>

<?php get_header(get_post_meta($post->ID, 'header_choice_select', true)); ?>

	
    
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "revolution-slider") { 
		}
	?>
    
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "notitle") { 
		echo '<hr>';
		}
	?>
    
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "") { 
	?>
        <!--BREADCRUMB / TITLE-->
        <section id="title-wrapper" class="title-breadcrumb">
            <div class="inside">
                <?php 
                    valise_get_page_title(); 
                    valise_breadcrumb(); 
                ?>
            </div>
        </section>
	<?php
		}
	?>
    

	<section class="theme-pages">
		<div class="inside clear">

			<!--LEFT CONTAINER-->
			<div class="right-content left">
            
            	<div class="column column-1-3">
                    <h3>Pages</h3>
                    <div id="sitemap_menu">
                        <ul><?php wp_list_pages('title_li=' ); ?></ul>
                    </div>
                </div>
                        
            	<div class="column column-1-3">
                    <h3>Categories</h3>
                    <ul><?php wp_list_categories('title_li='); ?></ul>
                </div>
	
				<div class="column column-1-3 column-last">
                    <h3>Archives</h3>
                    <ul>
                        <?php wp_get_archives('type=monthly&show_post_count=true'); ?>
                    </ul>   
                </div>              
                
			</div>

			<!--RIGHT SIDEBAR-->
			<div class="left-sidebar right">
				<?php
					if($sidebar_choice == 'Default') { get_sidebar(); } else { valise_get_custom_sidebar(); }
				?>
			</div>

		</div>
	</section>

<?php get_footer(); ?>