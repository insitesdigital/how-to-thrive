<form id="searchform" role="search" method="get" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div>
        <input value="<?php if(ot_get_option('valise_search_input')!= "") { echo ot_get_option('valise_search_input'); } else { _e( 'Search...', CSB_THEME_NAME ); } ?>" name="s" id="s" type="text" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">
        <input id="searchsubmit" value="Search" type="submit">
    </div>
</form>