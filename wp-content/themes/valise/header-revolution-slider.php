<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="keywords" content="<?php echo ot_get_option('valise_meta_keywords'); ?>" />   
<meta name="description" content="<?php echo ot_get_option('valise_meta_descriptions'); ?>" /> 
<?php
	if(ot_get_option('valise_site_layout')) {
		?><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" /><?php
	}
?>
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />


	<?php if (ot_get_option('valise_favicon')){
		echo '<link rel="shortcut icon" href="'. ot_get_option('valise_favicon') .'" />';
	} 

	if (ot_get_option('ipad_retina_icon')){
		echo '<link rel="apple-touch-icon" sizes="144x144" href="'. ot_get_option('ipad_retina_icon') .'" >';
	} 

	if (ot_get_option('iphone_retina_icon')){
		echo '<link rel="apple-touch-icon" sizes="114x114" href="'. ot_get_option('iphone_retina_icon') .'" >';
	}

	if (ot_get_option('ipad_icon')){
		echo '<link rel="apple-touch-icon" sizes="72x72" href="'. ot_get_option('ipad_icon') .'" >';
	} 

	if (ot_get_option('iphone_icon')){
		echo '<link rel="apple-touch-icon" href="'. ot_get_option('iphone_icon') .'" >';
	} ?>

    <title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
    <?php echo ot_get_option('analytics_code'); ?>
    <?php wp_head(); ?>
</head>

<!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<body <?php $class=''; body_class( $class ); ?>>
	
	<!--CONTAINER-->
	<div id="<?php echo ot_get_option('theme_layout', 'full-width'); ?>">
    
    <!-- Top bar -->
	<?php if(ot_get_option('top_bar')) {
		echo '<div id="top-bar-wrapper">
			<div id="top-bar">';
				get_sidebar('top-left'); 
				get_sidebar('top-right');
			echo '<div class="clear"></div>
			</div>
		</div>';
	} ?>

		<header class="clear">
			<section class="logo-menu inside">
				<!--LOGO-->		
				<aside class="logo">
						<?php 
						$default_logo = ot_get_option('logo_upload');
						$retina_logo = ot_get_option('retina_logo_upload');
						if (!$default_logo){
							echo '<a href="'. home_url() .'">
									<img src="' . get_template_directory_uri() . '/img/logo.png" alt="', bloginfo('name') .'" />
								</a>';
						} else {
							if ($retina_logo){
								$retina_logo_width = ot_get_option('retina_logo_width');
								$retina_logo_width = str_replace("px", "", $retina_logo_width);
								$retina_logo_height = ot_get_option('retina_logo_height');
								$retina_logo_height = str_replace("px", "", $retina_logo_height);
								echo '<a href="'. home_url() .'">
									<img src="'. $default_logo .'" alt="', bloginfo('name') .'" class="default-logo" />
									<img src="'. $retina_logo .'" width="'. $retina_logo_width .'" height="'. $retina_logo_height .'" alt="', bloginfo('name') .'" class="retina-logo" />
								</a>';
							} else {
								echo '<a href="'. home_url() .'">
									<img src="'. $default_logo .'" alt="', bloginfo('name') .'" />
								</a>';
							}
						}
						
						?>
				</aside>

				<!--MENU-->
				<nav>
					<?php
						$walker = new valise_Walker; 
						wp_nav_menu( array( 'theme_location' => 'primary-menu', 'fallback_cb' => 'valise_menu_fallback', 'container_class' => 'menu clear', 'container_id' => 'dropdown', 'menu_id' => 'main-menu', 'menu_class' => 'sf-menu', 'walker' => $walker ) );
					?>
				</nav>		
			</section>

		</header>
        
         <!-- Subhead -->	
		<?php 
		
        if ( function_exists( 'putRevSlider' ) ) {
			if (get_post_meta($post->ID, 'rev_slider_custom')) {
				echo '<div id="subhead_full">'.
					putRevSlider(get_post_meta($post->ID, 'rev_slider_custom', true)).
				'</div>';
			} 
		} else {}
		
        ?>


		<!--CUSTOM SIDEBAR-->
		<?php
			global $options, $sidebar_choice;
			
			@$options = get_post_custom(get_the_ID());
			if(isset($options['custom_sidebar'])) {
				$sidebar_choice = $options['custom_sidebar'][0];
			}

			valise_portfolio_link();
			valise_blog_link();
		?>	