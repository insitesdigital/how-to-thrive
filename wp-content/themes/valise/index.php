<?php get_header(get_post_meta($post->ID, 'header_choice_select', true)); ?>

    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "revolution-slider") { 
		}
	?>
    
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "notitle") { 
		echo '<hr>';
		}
	?>
    
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "") { 
	?>
        <!--BREADCRUMB / TITLE-->
        <section id="title-wrapper" class="title-breadcrumb">
            <div class="inside">
                    <?php
                    	if( get_post_meta( $post->ID, "pages_title", true ) != "") { 
							?><h2 class="ptitle"><?php echo stripslashes(get_post_meta( $post->ID, "pages_title", true )); ?></h2><?php 
						}else {
							?><h2 class="ptitle"><?php the_title(); ?></h2><?php
						}
                    ?> 
                <?php 
                    valise_breadcrumb(); 
                ?>
            </div>
        </section>
	<?php
		}
	?>

	<!--BLOG FULLWIDTH-->
	<section class="blog-page">
		<div class="inside">
			<div class="masonry clear">
				<?php
					valise_get_home_pagination();
					$blog_limit = ot_get_option('valise_num_blog');

					$args = array( 'post_type' => 'post', 'posts_per_page' => $blog_limit, 'paged' => $paged );		
					$wp_query = new WP_Query( $args );	
					for($i = 1; $wp_query->have_posts(); $i++) { 							
						$wp_query->the_post();			
						$columns = 3;	
						$class = 'blog-list ';
						$class .= ($i % $columns == 0) ? 'last' : '';
						
						get_template_part( 'includes/templates/loop', 'grid' );

					};						
				?>
			</div>
			<?php
				if(ot_get_option('valise_blog_pnav')=="Next Previous Link") { valise_next_previous(); }else { valise_pagination(); }
			?>
		</div>
	</section>

<?php get_footer(); ?>