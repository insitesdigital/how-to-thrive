<?php get_header(); ?>

        <!--BREADCRUMB / TITLE-->
        <section id="title-wrapper" class="title-breadcrumb">
            <div class="inside">
                <h2 class="ptitle"><?php $term = $wp_query->queried_object; echo $term->name; ?></h2>
                <?php 
                    valise_breadcrumb(); 
                ?>
            </div>
        </section>

	<!--PORTFOLIO-->
	<section class="project-page">
		<div class="inside clear">

			<?php valise_get_portfolio_category(); ?>

			<div class="masonry clear">
				<?php
					valise_get_home_pagination();
					$portfolio_order = ot_get_option('valise_album_order');
					$portfolio_orderby = ot_get_option('valise_album_orderby');
					$portfolio_limit = ot_get_option('valise_num_portfolio');

					$args = array( 'post_type' => 'portfolio', 'taxonomy' => 'portfolio_categories', 'orderby' => $portfolio_orderby, 'order' => $portfolio_order, 'posts_per_page' => $portfolio_limit, 'term' => $term->slug, 'meta_query' => array( array( 'key' => 'exclude', 'value' => 'on', 'compare' => '!=' ) ) );						
					$wp_query = new WP_Query( $args );	

					for($i = 1; $wp_query->have_posts(); $i++) { 							
						$wp_query->the_post();			
						$columns = 3;	
						$class = 'project-list three-cols ';
						$class .= ($i % $columns == 0) ? 'last' : '';
						
						get_template_part( 'includes/templates/loop', 'portfolio' );

					};						
				?>
			</div>
			<?php
				if(ot_get_option('valise_portfolio_pnav')=="Next Previous Link") { valise_next_previous(); }else { valise_pagination(); }
			?>

		</div>
	</section>

<?php get_footer(); ?>