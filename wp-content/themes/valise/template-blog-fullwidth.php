<?php
/*
Template Name: Blog - Fullwidth
*/
?>

<?php get_header(get_post_meta($post->ID, 'header_choice_select', true)); ?>

    
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "revolution-slider") { 
		}
	?>
    
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "notitle") { 
		echo '<hr>';
		}
	?>
    
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "") { 
	?>
        <!--BREADCRUMB / TITLE-->
        <section id="title-wrapper" class="title-breadcrumb">
            <div class="inside">
                <?php 
                    valise_get_page_title(); 
                    valise_breadcrumb(); 
                ?>
            </div>
        </section>
	<?php
		}
	?>

	<!--BLOG SIDEBAR-->
	<section class="blog-page">
		<div class="inside clear">

			<!--LEFT CONTAINER-->
			<div class="right-content <?php if(ot_get_option('valise_blog_sidebar')=="Right") { echo "left"; } ?>">
				<div class="clear">
					<?php
						valise_get_home_pagination();
						$blog_limit = ot_get_option('valise_num_blog');

						$args = array( 'post_type' => 'post', 'posts_per_page' => $blog_limit, 'paged' => $paged );		
						$wp_query = new WP_Query( $args );	
						while ($wp_query->have_posts()) : $wp_query->the_post(); 						
							get_template_part( 'includes/templates/loop', 'fullwidth' );
						endwhile;							
					?>
				</div>
				<?php
					if(ot_get_option('valise_blog_pnav')=="Next Previous Link") { valise_next_previous(); }else { valise_pagination(); }
				?>
			</div>

			<!--RIGHT SIDEBAR-->
			<div class="left-sidebar <?php if(ot_get_option('valise_blog_sidebar')=="Right") { echo "right"; } ?>">
				<?php
					if($sidebar_choice == "Default") { get_sidebar( 'blog' ); } else { valise_get_custom_sidebar(); }
				?>
			</div>

		</div>
	</section>

<?php get_footer(); ?>