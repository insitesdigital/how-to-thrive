<?php
	include get_template_directory() . '/includes/lib/custom-posts.php';
	include get_template_directory() . '/includes/lib/custom-widgets.php';
	include get_template_directory() . '/includes/widgets/widget-hire-me.php';
	include get_template_directory() . '/includes/lib/custom-functions.php';
	
	// Admin
	include get_template_directory() . '/includes/theme-options/main.php';
	include get_template_directory() . '/includes/theme-options/ot-loader.php';
	include get_template_directory() . '/includes/theme-options/assets/theme-options.php';
	include get_template_directory() . '/includes/theme-options/assets/theme-options-config.php';
	include get_template_directory() . '/includes/theme-options/assets/google-fonts.php';
	include get_template_directory() . '/includes/theme-options/assets/meta-boxes.php';
	
	// Include basic functions & scripts
	include get_template_directory() . '/includes/basic-functions.php';
	
	// Shortcodes
	include get_template_directory() . '/includes/shortcodes/shortcodes.php';

	/**********************************************
	REGISTER STYLES AND SCRIPTS
	***********************************************/

	function valise_script_styles_reg () {
		wp_register_style( 'style', get_stylesheet_directory_uri() . '/style.css' );	
		wp_register_style( 'responsive', get_stylesheet_directory_uri() . '/responsive.css' );	
		wp_register_style( 'dosis', 'http://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800&subset=latin,latin-ext' );			
		wp_register_style( 'opensans', 'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,latin-ext,cyrillic-ext,cyrillic,greek-ext,greek,vietnamese' );			
		wp_register_style( 'bitter', 'http://fonts.googleapis.com/css?family=Bitter:400,700,400italic&subset=latin,latin-ext' );			
		wp_register_style('font-awesome', get_template_directory_uri() . '/includes/font-awesome/css/font-awesome.min.css');
		wp_register_script( 'validate', get_template_directory_uri() . '/js/jquery.validate.min.js', array(), '', true );
		wp_register_script( 'mobile-menu', get_template_directory_uri() . '/js/jquery.mobilemenu.js', array(), '', true );	
		wp_register_script( 'easing', get_template_directory_uri() . '/js/jquery.easing-1.3.min.js', array(), '', true );	
		wp_register_script( 'masonry', get_template_directory_uri() . '/js/jquery.masonry.min.js', array(), '', true );		
		wp_register_script( 'retina', get_template_directory_uri() . '/js/retina.js', array(), '', true );	
		wp_register_script( 'backstretch', get_template_directory_uri() . '/js/jquery.backstretch.min.js', array(), '', true );
		wp_register_script( 'custom-js', get_template_directory_uri() . '/js/custom.js', array(), '', true );	
		
		/*COUNTER*/
		wp_register_script( 'counter-jquery', get_template_directory_uri() . '/js/counter/jquery-1.js', array(), '', true );
		wp_register_script( 'counter', get_template_directory_uri() . '/js/counter/counter.js', array(), '', true );
		
		/*SUPERFISH DROPDOWN MENU*/
		wp_register_style( 'superfish-style', get_template_directory_uri() . '/js/superfish/superfish.css' );
		wp_register_script( 'superfish', get_template_directory_uri() . '/js/superfish/jquery.superfish.min.js', array(), '', true );

		/*FLEXSLIDER*/
		wp_register_style( 'flexslider-style', get_template_directory_uri() . '/js/flexslider/flexslider.css' );		
		wp_register_script( 'flexslider', get_template_directory_uri() . '/js/flexslider/jquery.flexslider-min.js', array(), '', true );

		/*HTML5 VIDEO PLAYER*/
		wp_register_style( 'videojs-css', get_template_directory_uri() . '/js/video/video-js.css' );	
		wp_register_script( 'videojs', get_template_directory_uri() . '/js/video/video.js', array(), '', true );

		/*SCROLLPANE*/
		wp_register_style( 'scrollpane-style', get_template_directory_uri() . '/js/scrollpane/jquery.mCustomScrollbar.css' );	
		wp_register_script( 'scrollpane-mouse', get_template_directory_uri() . '/js/scrollpane/jquery.mousewheel.min.js', array(), '', true );
		wp_register_script( 'scrollpane', get_template_directory_uri() . '/js/scrollpane/jquery.mCustomScrollbar.min.js', array(), '', true );		
		wp_register_script( 'scrollpane-custom', get_template_directory_uri() . '/js/scrollpane/mCustomScrollbar.js', array(), '', true );	

		/*GOOGLE MAP*/
		wp_register_script( 'gmap-sensor', 'http://maps.google.com/maps/api/js?sensor=true' );
		wp_register_script( 'gmap', get_template_directory_uri() . '/js/gmaps.js' );
		
		// Dynamic style
		wp_enqueue_style('dynamic-style', get_template_directory_uri() . '/dynamic-style.php', false, '', 'all');
				
		/*ENQUEUE STYLE*/
		wp_enqueue_style( 'style' );
		wp_enqueue_style( 'dosis' );
		wp_enqueue_style( 'opensans' );
		wp_enqueue_style( 'bitter' );
		wp_enqueue_style( 'font-awesome' );
		wp_enqueue_style( 'superfish-style' );
		wp_enqueue_style( 'scrollpane-style' );
		
		/*ENQUEUE SCRIPT*/
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'counter-jquery' );
		wp_enqueue_script( 'counter' );
		wp_enqueue_script( 'superfish' );
		wp_enqueue_script( 'easing' );
		wp_enqueue_script( 'retina' );
		wp_enqueue_script( 'scrollpane-mouse' );
		wp_enqueue_script( 'scrollpane' );
		wp_enqueue_script( 'scrollpane-custom' );
		wp_enqueue_script( 'backstretch' );
		wp_enqueue_style( 'flexslider-style' );
		wp_enqueue_script( 'flexslider' );
		
		
		if (is_singular()) wp_enqueue_script('comment-reply');

		if(is_page_template('template-contact.php')) {
			wp_enqueue_script( 'validate' );
			wp_enqueue_script( 'gmap-sensor' );
			wp_enqueue_script( 'gmap' );

			//GOOGLE MAP 
			function google_map() {
				$lat = get_option('valise_map_lat');
				$lng = get_option('valise_map_lng');
				$marker_tip = get_option('valise_map_marker');
				$marker_window = get_option('valise_map_window');
				?>
				<script type="text/javascript">
					(function($) {
					  	"use strict";
					  	var map;
					    $(document).ready(function(){
					    	map = new GMaps({
						        div: '#map',
						        lat: <?php if($lat == "") { echo "-12.043333"; }else { echo $lat; } ?>,
	  							lng: <?php if($lng == "") { echo "-77.028333"; }else { echo $lng; } ?>
					      	});
					      	map.addMarker({
						        lat: <?php if($lat == "") { echo "-12.043333"; }else { echo $lat; } ?>,
	  							lng: <?php if($lng == "") { echo "-77.028333"; }else { echo $lng; } ?>,
						        title: '<?php if($marker_tip == "") { echo "Add your marker title window"; }else { echo $marker_tip; } ?>',
						        infoWindow: {
							    	content: '<p><?php if($marker_window == "") { echo "Please add your text marker here"; }else { echo $marker_window; } ?></p>'
							    }
					      	});
					    });
					})(jQuery);					
				</script>
				<?php
			}
			add_action( 'wp_head', 'google_map' );
		}

		if(is_singular('portfolio')) {
			wp_enqueue_style( 'touch-style' );
			wp_enqueue_script( 'touch' );
		}

		if(is_page_template('template-portfolio-2cols.php') || is_page_template('template-portfolio-3cols.php') || is_page_template('template-portfolio-4cols.php') || taxonomy_exists('portfolio_categories')) {
			wp_enqueue_script( 'masonry' );				
		}

		if(is_single() || is_archive() || is_category() || is_search()) {
			wp_enqueue_style( 'videojs-css' );
			wp_enqueue_script( 'videojs' );
		}
		
		if(is_page_template('template-blog-fullwidth.php') || is_page_template('template-blog-grid-sidebar.php') || is_page_template('template-blog-list-sidebar.php')) {
			wp_enqueue_style( 'videojs-css' );
			wp_enqueue_script( 'videojs' );
		}

		if(ot_get_option('valise_site_layout')) {
			wp_enqueue_style( 'responsive' );		
			wp_enqueue_script( 'mobile-menu' );	
		}

		wp_enqueue_script( 'custom-js' );
		
	}
	add_action( 'wp_enqueue_scripts', 'valise_script_styles_reg' );


	/**********************************************
	CHANGE DEFAULT SITE TITLE 
	***********************************************/

	function valise_change_default_site_title( $title ){
		$screen = get_current_screen();
		if('portfolio' == $screen->post_type) {
			$title = 'Enter portfolio name';
		}elseif('services' == $screen->post_type) {
			$title = 'Enter services name';
		}elseif('testimonials' == $screen->post_type) {
			$title = 'Enter name';
		}
		return $title;
	}

	add_filter( 'enter_title_here', 'valise_change_default_site_title' );


	/**********************************************
	MAIN MENU FALLBACK
	***********************************************/

	function valise_menu_fallback() {
		$class = "";
		if(is_front_page()){ $class="current_page_item"; }
		?>
		<div id="dropdown" class="menu clear">
			<ul id="menu-main-nav" class="sf-menu">
				<li class="<?php echo $class; ?>">
					<a href="<?php echo home_url(); ?>"><?php if(get_option('valise_home_menu')!="") { echo get_option('valise_home_menu'); } else { _e( 'Home', CSB_THEME_NAME ); } ?></a>
				</li>
				<?php wp_list_pages( 'title_li=&sort_column=menu_order' ); ?>
			</ul>
		</div>
		<?php
	}


	/*****************************************************
	THEME SUPPORTS, EDITOR, POST FORMATS AND BACKGROUND
	*****************************************************/

	add_theme_support( 'automatic-feed-links' );
	add_editor_style();
	//add_theme_support( 'custom-background' );
	add_theme_support( 'post-formats', array( 'image', 'video', 'gallery', 'quote' ) );
	
	
	/**********************************************
	CUSTOM MENUS
	***********************************************/
	
	add_action( 'init', 'valise_register_my_menus' );
	function valise_register_my_menus() {
		register_nav_menus(
			array(
				'primary-menu' => 'Primary Menu',	
				'secondary-menu' => 'Footer Menu',	
			)
		);
	}
	
	/**********************************************
	DO SHORTCODES
	***********************************************/

	add_filter('widget_text', 'do_shortcode');
	add_filter('the_content', 'do_shortcode');


	/**********************************************
	SET MAXIMUM CONTENT WIDTH
	***********************************************/

	if ( ! isset( $content_width ) ) $content_width = 1170;


	/**********************************************
	LANGUAGE LOCALIZATION
	***********************************************/

	define('CSB_THEME_NAME', 'valise_lang');

	add_action('after_setup_theme', 'valise_language_setup');
	function valise_language_setup(){		
		load_theme_textdomain(CSB_THEME_NAME, get_template_directory() . '/lang');
	}
	

	/**********************************************
	POST THUMBNAIL
	***********************************************/

	if ( function_exists( 'add_theme_support' ) ) { 
		add_theme_support( 'post-thumbnails', array( 'post', 'portfolio' ) );
		set_post_thumbnail_size( 270, 237, true ); 
		add_image_size( 'project-thumb-2cols', 570, 297, true );
		add_image_size( 'project-thumb-3cols', 370, 237, true );
		add_image_size( 'project-large', 770, 9999 );
		add_image_size( 'project-xlarge', 1170, 9999 );
		add_image_size( 'blog-thumb', 364, 210, true );
		add_image_size( 'blog-medium', 370, 210, true );
		add_image_size( 'blog-large', 770, 9999 );
		
		add_image_size( 'blog-fullwidth', 770, 380, true );
		
		add_image_size( 'cs-size-large', 581, 297, true );
		add_image_size( 'cs-size-medium', 288, 147, true );
		add_image_size( 'cs-size-verticle', 288, 297, true );
		add_image_size( 'cs-size-horizontal', 581, 147, true );
	}


	/**********************************************
	GET PAGE CUSTOM TITLE
	***********************************************/

	function valise_get_page_title() {
		global $post;
		if( get_post_meta( $post->ID, "pages_title", true ) != "") { 
			?><h2 class="ptitle"><?php echo stripslashes(get_post_meta( $post->ID, "pages_title", true )); ?></h2><?php 
		}else {
			?><h2 class="ptitle"><?php the_title(); ?></h2><?php
		}
	}


	/**********************************************
	GET MENU DESCRIPTIONS
	***********************************************/

	class valise_Walker extends Walker_Nav_Menu {
		function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$classes     = empty ( $item->classes ) ? array () : (array) $item->classes;
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . ' class="' . $class_names .'">';
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		if ( $item->description ) {
		  $item_output .= '<span class="menu-description"><i class="' . $item->description . ' icon-large"></i></span>';
		}
		$item_output .= $args->link_before . '<span class="menu-title">' . apply_filters( 'the_title', $item->title, $item->ID ) . '</span>' . $args->link_after;
		
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}
	}


	/**********************************************
	GET BREADCRUMB
	***********************************************/

	function valise_breadcrumb() {
        ?>
        <?php if (!ot_get_option('disable_breadcrumbs')) { ?>
    	<div class="breadcrumbs">
    		<?php 
    			if (!is_front_page()) {
		            ?><a href="<?php echo home_url(); ?>"><?php _e( 'Home', CSB_THEME_NAME ); ?></a><?php

		            if(is_archive()) {
		            	echo " / ";
		            	if ( is_day() ) :
							printf( __( '%s', CSB_THEME_NAME ), get_the_date() ); 
						elseif ( is_month() ) :
							printf( __( '%s', CSB_THEME_NAME ), get_the_date( __( 'F Y', 'monthly archives date format', CSB_THEME_NAME ) ) ); 
						elseif ( is_year() ) :
							printf( __( '%s', CSB_THEME_NAME ), get_the_date( __( 'Y', 'yearly archives date format', CSB_THEME_NAME ) ) );
						elseif (is_category()) :
							single_cat_title(); 
						elseif( is_tag() ) :
							printf( single_tag_title() ); 
						elseif( is_tax() ) :
							$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
							echo $term->name;
						endif;
		            }elseif (is_category() || is_single()) {
		                echo " / ";
		                the_category(', ');
		                if (is_single()) {
		                	if(is_singular('portfolio')) {
		                		the_title();
		                	}else {
		                		echo " / ";
		                		the_title();
		                	}		                	
		                }
		            }elseif (is_page()) { 
		            	echo " / "; 
		            	echo the_title(); 
		            }elseif(is_search()) {
		            	echo " / "; 
		            	_e( 'Search', CSB_THEME_NAME );
		            }elseif(is_404()) {
		            	echo " / "; 
		            	_e( 'Page Not Found', CSB_THEME_NAME );
		            }
		        }else { _e( 'Home', CSB_THEME_NAME ); }
    		?>
    	</div>
        <?php
	}
	}


	/**********************************************
	GET RECEIPIANT IP ADDRESS
	***********************************************/

	function valise_get_the_ip() {
	    if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
	        return $_SERVER["HTTP_X_FORWARDED_FOR"];
	    }
	    elseif (isset($_SERVER["HTTP_CLIENT_IP"])) {
	        return $_SERVER["HTTP_CLIENT_IP"];
	    }
	    else {
	        return $_SERVER["REMOTE_ADDR"];
	    }
	}


	/**********************************************
	FOR PAGINATION WORKING ON STATIC HOMEPAGE
	***********************************************/

	function valise_get_home_pagination() {
		global $paged, $wp_query, $wp;
		$args = wp_parse_args($wp->matched_query);
		if ( !empty ( $args['paged'] ) && 0 == $paged ) {
			$wp_query->set('paged', $args['paged']);
		  	$paged = $args['paged'];
		}
	}


	/**********************************************
	GET PORTFOLIO CATEGORIES
	***********************************************/

	function valise_get_portfolio_category() {
		global $portfolio_page_url;
		if(get_option('valise_hide_portfolio_drop')!="true") {
			?>
				<div class="cat-toogles clear">
					<ul class="cat-list">
						<li <?php if(is_page_template('template-portfolio-2cols.php') || is_page_template('template-portfolio-3cols.php') || is_page_template('template-portfolio-4cols.php')) { echo "class='current-cat'"; } ?>>
							<a href="<?php if(ot_get_option('valise_all_link')) { echo ot_get_option('valise_all_link'); } ?>"><?php if(ot_get_option('valise_pcategory_label')) { echo ot_get_option('valise_pcategory_label'); } else { _e( 'All', CSB_THEME_NAME ); } ?></a>
						</li>
						<?php
							$args = array( 'taxonomy' => 'portfolio_categories', 'style' => 'list', 'title_li' => '', 'hierarchical' => false, 'order' => 'DESC', 'orderby' => 'title' );
							wp_list_categories ( $args );							
						?>	
					</ul>
				</div>
			<?php
		}
	}	


	/**********************************************
	GET PAGES LINK
	***********************************************/

	function valise_portfolio_link() {
		global $portfolio_page_url;
		$portfolio_pages = get_pages(array('meta_key' => '_wp_page_template', 'meta_value' => 'template-portfolio-3cols.php'));
		foreach($portfolio_pages as $page){
			$portfolio_page_id = $page->ID;
			$portfolio_page_url = get_permalink($portfolio_page_id);
		}
	}

	function valise_blog_link() {
		global $blog_page_url;
		$blog_pages = get_pages(array('meta_key' => '_wp_page_template', 'meta_value' => 'template-blog-fullwidth.php'));
		foreach($blog_pages as $page){
			$blog_page_id = $page->ID;
			$blog_page_url = get_permalink($blog_page_id);
		}
	}


	/**********************************************
	CUSTOM EXCERPT LENGTH
	***********************************************/

	function valise_custom_excerpt_length( $length ) {
		return 30;
	}
	add_filter( 'excerpt_length', 'valise_custom_excerpt_length', 999 );

	function valise_custom_excerpt_more( $more ) {
		return ' ...';
	}
	add_filter( 'excerpt_more', 'valise_custom_excerpt_more' );


	/**********************************************
	REMOVE DEFAULT COMMENT FIELDS
	***********************************************/

	function valise_remove_comment_fields($arg) {
	    $arg['url'] = '';
	    return $arg;
	}
	add_filter('comment_form_default_fields', 'valise_remove_comment_fields');

	
	/**********************************************
	CUSTOM COMMENT STYLE
	***********************************************/

	function valise_theme_comment($comment, $args, $depth) {
		$GLOBALS['comment'] = $comment; ?>
			
			<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
				<div class="parent clear" id="comment-<?php comment_ID(); ?>">
					
					<?php echo get_avatar( $comment, 80 ); ?>
                    
                    <div class="comment-details">
                        <h6>
                            <?php 
                                comment_author_link();
                            ?>
                        </h6> 
                        <span>
                            <?php comment_date(get_option('date_format')); _e( '&nbsp;at&nbsp;', CSB_THEME_NAME ); comment_time(get_option('time_format')); ?> 
                            <?php edit_comment_link('edit','&nbsp;',''); ?>                            
                        </span>	
                        <span><?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))); ?></span>
                    </div>
                        
					<div class="comments-p">
						
						<?php comment_text() ?> 
					</div>
					
					<?php 
						if ($comment->comment_approved == '0') : ?>
						<em><?php _e( 'Your comment is awaiting moderation.', CSB_THEME_NAME ); ?></em>
					<?php 
						endif; 					
					?>
				</div>	
			
			<?php
				$oddcomment = ( empty( $oddcomment ) ) ? 'class="alt" ' : '';
				paginate_comments_links();
	}


	/**********************************************
	CUSTOM PAGINATION
	***********************************************/

	function valise_pagination() {  
		global $wp_query;
		$big = 999999999;
		if($wp_query->max_num_pages == '1' ) {
		}else {
			echo "<div class=\"pagination clear\">";
		}
		echo paginate_links( array(
			'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
			'format' => '?paged=%#%',
			'prev_text' => __( '&laquo;', CSB_THEME_NAME ),
    		'next_text' => __( '&raquo;', CSB_THEME_NAME ),
			'current' => max( 1, get_query_var('paged') ),
			'total' => $wp_query->max_num_pages,
			'type' => 'list'
		));
		if($wp_query->max_num_pages == '1' ) {
		}else {
			echo "</div>";
		}
	}


	/**********************************************
	CUSTOM NEXT PREVIOUS LINK
	***********************************************/

	function valise_next_previous() {
		?>
		<div class="pagination">
			<?php
			    global $wp_query, $paged;		    
			    if ($paged > 1) {
			    	?><div class="alignleft"><a href="<?php previous_posts(); ?>">&larr; <?php _e( 'Previous', CSB_THEME_NAME ); ?></a></div><?php
			    }
			    if ($wp_query->max_num_pages == 1) {		    		
		    	}elseif ($paged < $wp_query->max_num_pages) {
		    		?><div class="alignright"><a href="<?php next_posts(); ?>"><?php _e( 'Next', CSB_THEME_NAME ); ?> &rarr;</a> </div><?php
		    	}
			?>
		</div>
		<?php
	}


	/**********************************************
	FIX PAGINATION FOR TAXONOMIES
	***********************************************/

	$option_posts_per_page = get_option( 'posts_per_page' );
	
	function valise_modify_posts_per_page() {
	    add_filter( 'option_posts_per_page', 'valise_option_posts_per_page' );
	}
	add_action( 'init', 'valise_modify_posts_per_page', 0);

	function valise_option_posts_per_page( $value ) {
	    global $option_posts_per_page, $wp_query;
	    if ( is_tax( 'portfolio_categories') ) {
	        return $wp_query->max_num_pages;
	    } else {
	        return $option_posts_per_page;
	    }
	}


	/**********************************************
	SEARCH FILTER : BLOG POST ONLY
	***********************************************/

	function valise_searchfilter($query) {
	    if ( !$query->is_admin && $query->is_search) {
	         $query->set('post_type', 'post');
	    }
	    return $query;
	}	 
	add_filter('pre_get_posts','valise_searchfilter');


	/**********************************************
	REMOVE & ADD NEW FIELD IN USER PROFILE
	***********************************************/

	function valise_remove_aim( $contactmethods ) {
		unset($contactmethods['aim']);
		unset($contactmethods['jabber']);
		unset($contactmethods['yim']);
		return $contactmethods;
	}
	add_filter('user_contactmethods','valise_remove_aim',10,1);

	function valise_add_twitter_facebook( $contactmethods ) {
	    $contactmethods['twitter'] = 'Twitter';
	    $contactmethods['facebook'] = 'Facebook';
	    return $contactmethods;
	}
	add_filter('user_contactmethods','valise_add_twitter_facebook',10,1);


	/**********************************************
	DISPLAY LIST OF WIDGETS
	***********************************************/

	add_action( 'add_meta_boxes', 'valise_add_sidebar_metabox' );
	add_action( 'save_post', 'valise_save_sidebar_postdata' );

	function valise_add_sidebar_metabox() {
		add_meta_box( 'custom_sidebar', __( 'Custom Sidebar', CSB_THEME_NAME ), 'valise_custom_sidebar_callback', 'page', 'side' );
		add_meta_box( 'custom_sidebar', __( 'Custom Sidebar', CSB_THEME_NAME ), 'valise_custom_sidebar_callback', 'post', 'side' ); 
	}

	/*SIDEBAR CALLBACK*/

	function valise_custom_sidebar_callback( $post ) {
		global $wp_registered_sidebars;
		$custom = get_post_custom($post->ID);

		if(isset($custom['custom_sidebar']))
			$val = $custom['custom_sidebar'][0];
		else
			$val = "Default";

			wp_nonce_field( plugin_basename( __FILE__ ), 'custom_sidebar_nonce' );

			$output = '<p><small><label for="myplugin_new_field">'.__("Choose a sidebar to display except footer columns", CSB_THEME_NAME ).'</label></small></p>';
			$output .= "<select name='custom_sidebar'>";

		$output .= "<option";
		if($val == "Default")
			$output .= " selected='selected'";
			$output .= " value='Default'>".__('Default', CSB_THEME_NAME)."</option>";

			foreach($wp_registered_sidebars as $sidebar_id => $sidebar) {
				$output .= "<option";
				if($sidebar_id == $val)
					$output .= " selected='selected'";
					$output .= " value='".$sidebar_id."'>".$sidebar['name']."</option>";
			}

		$output .= "</select>";
		echo $output;
	}

	/*SAVE SIDEBAR DATA*/

	function valise_save_sidebar_postdata( $post_id ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return;

		if ( !wp_verify_nonce( @$_POST['custom_sidebar_nonce'], plugin_basename( __FILE__ ) ) )
		 	return;

		if ( !current_user_can( 'edit_page', $post_id ) )
			return;

		$data = $_POST['custom_sidebar'];
		update_post_meta($post_id, "custom_sidebar", $data);
	}


	/**********************************************
	GET CUSTOM SIDEBAR
	***********************************************/

	function valise_get_custom_sidebar() {
		global $sidebar_choice;

		if($sidebar_choice == "sidebar") {
			get_sidebar();
		}elseif($sidebar_choice == "blog_sidebar") {
			get_sidebar( 'blog' );
		}elseif($sidebar_choice == "contact_sidebar") {
			get_sidebar( 'contact' );
		}else {
			get_sidebar();
		}
	}


	/**********************************************
	ADD POST THUMBNAIL SIZE IN MEDIA UPLOAD
	***********************************************/

	function valise_get_additional_image_sizes() {
		$sizes = array();
		global $_wp_additional_image_sizes;
		if ( isset($_wp_additional_image_sizes) && count($_wp_additional_image_sizes) ) {
			$sizes = apply_filters( 'intermediate_image_sizes', $_wp_additional_image_sizes );
			$sizes = apply_filters( 'valise_get_additional_image_sizes', $_wp_additional_image_sizes );
		}
		return $sizes;
	}

	function additional_image_size_input_fields( $fields, $post ) {
		if ( !isset($fields['image-size']['html']) || substr($post->post_mime_type, 0, 5) != 'image' )
			return $fields;

		$sizes = valise_get_additional_image_sizes();
		if ( !count($sizes) )
			return $fields;

		$items = array();
		foreach ( array_keys($sizes) as $size ) {
			$downsize = image_downsize( $post->ID, $size );
			$enabled = $downsize[3];
			$css_id = "image-size-{$size}-{$post->ID}";
			$label = apply_filters( 'image_size_name', $size );

			$html  = "<div class='image-size-item'>\n";
			$html .= "<input type='radio' " . disabled( $enabled, false, false ) . "name='attachments[{$post->ID}][image-size]' id='{$css_id}' value='{$size}' />\n";
			$html .= "<label for='{$css_id}'>{$label}</label>\n";
			if ( $enabled )
				$html .= "<label for='{$css_id}' class='help'>" . sprintf( "(%d x %d)", $downsize[1], $downsize[2] ). "</label>\n";
			$html .= "</div>";

			$items[] = $html;
		}

		$items = join( "\n", $items );
		$fields['image-size']['html'] = "{$fields['image-size']['html']}\n{$items}";

		return $fields;
	}

	add_filter( 'attachment_fields_to_edit', 'additional_image_size_input_fields', 11, 2 );


	/**********************************************
	CUSTOM THEME STYLESHEETS
	***********************************************/

	function valise_theme_custom_styles() {
		?><style type="text/css"><?php	

		/*PATTERNS*/

		if(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Gray Lines'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Select Pattern'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Noise Lines'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Tiny Grid'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Bullseye'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Gray Paper'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Norwegian Rose'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Subtle Net'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Polyester Lite'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Absurdity'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'White Bed Sheet'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Subtle Stripes'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Light Mesh'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Rough Diagonal'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Arabesque'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Stack Circles'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Hexellence'); ?>') repeat top fixed fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'White Texture'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Concrete Wall'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Brush Aluminum'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Groovepaper'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Diagonal Noise'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Rocky Wall'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Whitey'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Bright flexs'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Freckles'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Wallpaper'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Project Paper'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Cubes'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Washi'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Dot Noise'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'xv'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Little Plaid'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Old Wall'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Connect'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Ravenna'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Smooth Wall'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Tapestry'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Psychedelic'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Scribble Light'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'GPlay'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Lil Fiber'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'First Aid'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Frenchstucco'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Light Wool'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Gradient flexs'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Escheresque'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Climpek'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Lyonnette'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Gray Floral'); ?>') repeat top fixed !important; }<?php
		}elseif(ot_get_option('valise_pattern')) {
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Reticular Tissue'); ?>') repeat top fixed !important; }<?php
		}else { 
			?>body { background:url('<?php echo get_template_directory_uri(); ?>/<?php echo ot_get_option('valise_pattern', 'Select Pattern'); ?>') repeat top fixed !important; }<?php
			}


		/*CUSTOM COLORS*/

		if(ot_get_option('valise_custom_css')) {
			echo ot_get_option('valise_custom_css');
		}

		?></style><?php
	}
	add_action( 'wp_head', 'valise_theme_custom_styles' );



	/**********************************************
	CUSTOM RESIZABLE BACKGROUND
	***********************************************/

	function valise_theme_custom_background() {
		global $post, $archive_bgimage;
		$bgimage = get_post_meta(@$post->ID, "page_bg_image", true);
		$archive_bgimage = ot_get_option('valise_archive_bgimage');
		$search_bgimage = ot_get_option('valise_search_bgimage');
		$notfound_bgimage = ot_get_option('valise_notfound_bgimage');

		if(is_archive()) {
			if($archive_bgimage != "") {
				?>
				<script type="text/javascript"> 
					jQuery(window).load(function() { jQuery.backstretch("<?php echo $archive_bgimage; ?>"); }); 
				</script>
				<?php	
			}else { default_img_bg(); }
		}elseif(is_search()) {
			if($search_bgimage != "") {
				?>
				<script type="text/javascript"> 
					jQuery(window).load(function() { jQuery.backstretch("<?php echo $search_bgimage; ?>"); }); 
				</script>
				<?php					
			}else { default_img_bg(); }
		}elseif(is_404()) {
			if($notfound_bgimage != "") {
				?>
				<script type="text/javascript"> 
					jQuery(window).load(function() { jQuery.backstretch("<?php echo $notfound_bgimage; ?>"); }); 
				</script>
				<?php						
			}else { default_img_bg(); }
		}elseif(is_page() || is_single()) {
			if($bgimage != "") {
				?>
				<script type="text/javascript"> 
					jQuery(window).load(function() { jQuery.backstretch("<?php echo $bgimage; ?>"); }); 
				</script>
				<?php					
			}else { default_img_bg(); }
		}else {
			default_img_bg();
		}

	}
	
	if(get_option('valise_theme_layout')=="Boxed") {
		add_action( 'wp_footer', 'valise_theme_custom_background' );
	}

	function default_img_bg() {
		$default_image = ot_get_option('valise_default_bgimage');

		 

		if(ot_get_option('valise_pattern')) {	
			if(ot_get_option('valise_bg_type') == 'image' ) {
				if($default_image) {
					?>
						<script type="text/javascript"> 
							jQuery(window).load(function() { jQuery.backstretch("<?php echo $default_image; ?>"); });
						</script>
					<?php	
				}else {
					?>
					<script type="text/javascript"> 
						jQuery(window).load(function() { jQuery.backstretch("<?php echo get_template_directory_uri(); ?>/img/default.jpg"); }); 
					</script>
					<?php
				}			
			}	
		}
	}	
?>