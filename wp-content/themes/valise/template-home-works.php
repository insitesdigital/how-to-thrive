<?php
/*
Template Name: Homepage + Featured Works
*/
?>


<?php get_header(); ?>

    <?php
		valise_welcome();
		valise_portfolio_list();
		valise_services_list();
	?>
	<section class="contents">
			<div class="testimonial_list">
				<div class="inside clear"><?php valise_testimonial_list(); ?></div>
            </div>
        <div class="inside clear">
			<div class=""><?php valise_blog_list(); ?></div>
		</div>
	</section>

<?php get_footer(); ?>