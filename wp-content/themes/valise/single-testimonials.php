<?php get_header(); ?>

        <!--BREADCRUMB / TITLE-->
        <section id="title-wrapper" class="title-breadcrumb">
            <div class="inside">
           
                 <?php
					if( get_post_meta( $post->ID, "pages_title", true ) != "") { 
						?><h2 class="ptitle"><?php echo stripslashes(get_post_meta( $post->ID, "pages_title", true )); ?></h2><?php 
					} elseif (post_password_required()) {
						echo '<h2 class="ptitle">Protected</h2>';
					} else {
						?><h2 class="ptitle"><?php the_title(); ?></h2><?php
					}
				?> 
                <?php valise_breadcrumb(); ?>
            </div>
        </section>

	<!--BLOG SIDEBAR-->
	<section class="blog-page">
		<div class="inside clear">

			<!--LEFT CONTAINER-->
			<div class="right-content <?php if(ot_get_option('valise_blog_sidebar')=="Right") { echo "left"; } ?>">
				<div class="clear">
					<?php
						if ( post_password_required() ) {
							?><div class="password-protected"><?php the_content(); ?></div><?php
						}else {
							while (have_posts()) : the_post(); 						
								?>
								<div class="blog-list-single">
                                    
                                    <div <?php post_class('single-desc'); ?>>
                                    
                                        <h2 class="testimonials-heading"><?php the_title(); ?></h2>
                                                                                                                   
                                        <div class="divider-arrow-down"></div> 
                                                                               
										<?php the_content(); ?>
                                        
                                        <?php wp_link_pages('before=<p class="post-navigation">&<span>after=</span></p>&pagelink=<span>%</span>'); ?>
                                        
									</div>
                                   
								</div>
								<?php
							endwhile;

							comments_template();
						}			
					?>
				</div>	

			</div>

			<!--RIGHT SIDEBAR-->
			<div class="left-sidebar <?php if(ot_get_option('valise_blog_sidebar')=="Right") { echo "right"; } ?>">
				<?php
					if($sidebar_choice == "Default") { get_sidebar( 'blog' ); } else { valise_get_custom_sidebar(); }
				?>
			</div>
		</div>
	</section>

<?php get_footer(); ?>