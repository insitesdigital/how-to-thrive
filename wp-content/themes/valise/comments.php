<?php
// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if ( post_password_required() ) { ?>
		<p class="nocomments"><?php _e('This post is password protected. Enter the password to view comments.', CSB_THEME_NAME); ?></p>
	<?php
		return;
	}
?>

<!--COMMENTS-->
<div class="comments" id="comments">
	<?php if ( have_comments() ) : ?>
	<h3>
		<?php if(ot_get_option('valise_comment_cheader')) { echo ot_get_option('valise_comment_cheader'); } else { _e( 'Latest comments', CSB_THEME_NAME ); } ?>
	</h3>
	<!--comments_number('No Response', 'One Response', '% Responses' );-->
	<ul class="comment-list">
 		<?php 
 			$args = array ('type' => 'comment', 'callback' => 'valise_theme_comment');
			wp_list_comments( $args ); 		
 		?>
 	</ul>
	 	<?php
		else :    
			if ('open' == $post->comment_status) : 
				?>
					<h3 class="error-color">
						<?php if(ot_get_option('valise_comment_notify')) { echo ot_get_option('valise_comment_notify'); } else { _e('No comments', CSB_THEME_NAME); } ?>
					</h3>
				<?php else : ?>            
					<h5><?php _e('Comments are closed.', CSB_THEME_NAME); ?></h5>    
			<?php endif; ?>
    <?php endif; ?>
</div>

<!--COMMENT FORM-->

<?php 
	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );

	$fields = array(
			'author' => '<li><input type="text" id="author" name="author" ' . $aria_req . ' value="' .
        esc_attr( $commenter['comment_author'] ) . '" tabindex="1" placeholder="' . __( 'Name', CSB_THEME_NAME ) . '" /></li>',
			'email' => '<li><input type="text" id="email" name="email" ' . $aria_req . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" tabindex="2" placeholder="' . __( 'Email', CSB_THEME_NAME ) . '"  /></li>',
			'URL' => '<li><input type="text" id="url" name="url" value="' . esc_attr( $commenter['comment_author_url'] ) . '" tabindex="3" placeholder="' . __( 'Website', CSB_THEME_NAME ) . '" /></li>'
	);

	if(ot_get_option('valise_comment_fheader')) { $comm = ot_get_option('valise_comment_fheader'); } else { $comm = "Leave a comment"; }

	$args = array(
		'fields' => apply_filters( 'comment_form_default_fields', $fields),
		'title_reply' => $comm,
		'cancel_reply_link' => 'Cancel reply',
		'comment_field' => '<li class="msg"><textarea id="comment" name="comment" ' . $aria_req . ' tabindex="4" placeholder="' . __( 'Your Comment', CSB_THEME_NAME ) . '"></textarea></li>',
		'label_submit' => __( 'Submit Comment', CSB_THEME_NAME ),
		'comment_notes_before' => '<ul>',
		'comment_notes_after' => '</ul>',
	);
	comment_form($args); 
?>
