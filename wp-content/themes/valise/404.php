<?php get_header(); ?>
    
    
        <!--BREADCRUMB / TITLE-->
        <section id="title-wrapper" class="title-breadcrumb">
		<div class="inside">
			<h2 class="ptitle"><?php if(ot_get_option('valise_notfound_header')!= "") { echo ot_get_option('valise_notfound_header'); } else { _e( 'Error 404', CSB_THEME_NAME ); } ?></h2>
			<?php 
				valise_breadcrumb(); 
			?>
		</div>
        </section>
   
	<section class="theme-pages">
    
		<div class="inside page-not-found">
            <img src="<?php echo get_template_directory_uri(); ?>/img/404.png" />
        	
            <div class="nf-box">
                <h4>
                    <?php 
                        if(ot_get_option('valise_notfound_heading')!= "") { echo ot_get_option('valise_notfound_heading'); } 
                        else { _e( '404 Error', CSB_THEME_NAME ); } 
                    ?>
                </h4>
                
                <p>
                <?php 
					if(ot_get_option('valise_notfound_text')!= "") { echo ot_get_option('valise_notfound_text'); } 
					else { _e( 'Sorry, but there was some problem finding the page you requested. Maybe that page was moved or deleted, perhaps you just mistyped the address. it happens. Return to', CSB_THEME_NAME ); } 
				?>
             	<a href="<?php echo home_url(); ?>" class="back-home">
                    <?php if(ot_get_option('valise_notfound_button')!= "") { echo ot_get_option('valise_notfound_button'); } else { _e( 'Home Page', CSB_THEME_NAME ); } ?>
                </a>.
            </div>
		</div>
        
        <div class="page-nf-line">
        </div>
        
	</section>

<?php get_footer(); ?>