	<div id="topright-widget-area" class="widget-area size-wrap">
		<?php
            if ( is_active_sidebar( 'top_right_sidebar' ) ) : dynamic_sidebar( 'top_right_sidebar' );
            else : _e( 'This is topright widget area, so please add widgets here...', CSB_THEME_NAME );
            endif;
        ?>
	</div>
