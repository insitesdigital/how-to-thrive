/**
 * jQuery Mobile Menu 
 * Turn unordered list menu into dropdown select menu
 * version 1.0(31-OCT-2011)
 * 
 * Built on top of the jQuery library
 *   http://jquery.com
 * 
 * Documentation
 * 	 http://github.com/mambows/mobilemenu
 */
(function($){
$.fn.mobileMenu = function(options) {
	
	var defaults = {
			defaultText: 'Go to Page...',
			className: 'select-menu',
			subMenuClass: 'sub-menu',
			subMenuDash: '&nbsp;&nbsp;&nbsp;'
		},
		settings = $.extend( defaults, options ),
		el = $(this);
	
	this.each(function(){
		// add class to submenu list
		el.find('ul').addClass(settings.subMenuClass);

		// Create base menu
		$('<select />',{
			'class' : settings.className
		}).insertAfter("#dropdown ul#main-menu");

		// Create default option
		$('<option />', {
			"value"		: '#',
			"text"		: settings.defaultText
		}).appendTo( '.' + settings.className );

		// Create select option from menu
		el.find('a').each(function(){
			var $this 	= $(this),
				optText	= '&nbsp;' + $this.find("span.menu-title").text(),
				optSub	= $this.parents( '.' + settings.subMenuClass ),
				len = optSub.length, dash;
			
			// if menu has sub menu
			if( $this.parents('ul').hasClass( settings.subMenuClass ) ) {
				dash = Array( len + 1 ).join( settings.subMenuDash );
				optText = dash + optText;
			}

			// Now build menu and append it
			$('<option />', {
				"value"	: this.href,
				"html"	: optText,
				"selected" : (this.href == window.location.href)
			}).appendTo( '.' + settings.className );

		}); // End el.find('a').each

		// Change event on select element
		$('.' + settings.className).change(function(){
			var locations = $(this).val();
			if( locations !== '#' ) {
				window.location.href = $(this).val();
			};
		});

	}); // End this.each

	return this;

};
})(jQuery);


//INITIALISE 

(function($) {
	"use strict";
	$("#dropdown ul#main-menu").mobileMenu({ 
		defaultText: 'Navigate to...',
	    className: 'select-menu',
	    subMenuDash: '&nbsp;&ndash;&nbsp;' 
	});
})(jQuery);