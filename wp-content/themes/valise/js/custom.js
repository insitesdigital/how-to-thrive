(function($) {
	"use strict";	

    /*SUPERFISH*/

    $('ul.sf-menu').superfish();


    /*HOVER*/

    $(".portfolio .pimage").hover(function(){
        $("img", this).stop().animate({opacity: 0.2},{queue:false,duration:400});
        $(".project-mask", this).stop().animate({opacity: 1},{queue:false,duration:600});
    }, function() {
        $("img", this).stop().animate({opacity: 1},{queue:false,duration:400});
        $(".project-mask", this).stop().animate({opacity: 0},{queue:false,duration:600});
    });

    $(".basic-attachment ul li").hover(function(){
        $("img", this).stop().animate({opacity: 0.7},{queue:false,duration:400});
        $(".project-mask", this).stop().animate({opacity: 1},{queue:false,duration:600});
    }, function() {
        $("img", this).stop().animate({opacity: 1},{queue:false,duration:400});
        $(".project-mask", this).stop().animate({opacity: 0},{queue:false,duration:600});
    });

    $(".fw-img").hover(function(){
        $("img", this).stop().animate({opacity: 0.4},{queue:false,duration:400});
        $(".blog-mask", this).stop().animate({opacity: 1},{queue:false,duration:600});
    }, function() {
        $("img", this).stop().animate({opacity: 1},{queue:false,duration:400});
        $(".blog-mask", this).stop().animate({opacity: 0},{queue:false,duration:600});
    });
	$(".gd-img").hover(function(){
        $("img", this).stop().animate({opacity: 0.4},{queue:false,duration:400});
        $(".blog-mask", this).stop().animate({opacity: 1},{queue:false,duration:600});
    }, function() {
        $("img", this).stop().animate({opacity: 1},{queue:false,duration:400});
        $(".blog-mask", this).stop().animate({opacity: 0},{queue:false,duration:600});
    });
	$(".lt-img").hover(function(){
        $("img", this).stop().animate({opacity: 0.4},{queue:false,duration:400});
        $(".blog-mask", this).stop().animate({opacity: 1},{queue:false,duration:600});
    }, function() {
        $("img", this).stop().animate({opacity: 1},{queue:false,duration:400});
        $(".blog-mask", this).stop().animate({opacity: 0},{queue:false,duration:600});
    });
	
	$(".gallery-post .slides").hover(function(){
        $("img", this).stop().animate({opacity: 1},{queue:false,duration:0});
        $(".gallery-post", this).stop().animate({opacity: 1},{queue:false,duration:0});
    }, function() {
        $("img", this).stop().animate({opacity: 1},{queue:false,duration:0});
        $(".gallery-post", this).stop().animate({opacity: 1},{queue:false,duration:0});
    });
	
    /*FADE TIMEOUT*/

    setTimeout(function(){
        $(".sent").fadeOut("slow", function () {
            $(".sent").remove();
        });
    }, 5000);


    /*SCROLLTOP*/

    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#toTop').fadeIn();
            } else {
                $('#toTop').fadeOut();
            }
        });

        $("#toTop").click(function(){
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });  
    });

})(jQuery);