(function($) {
    "use strict";
    
    $(window).load(function() {        
        $(".horScroll").mCustomScrollbar({
            horizontalScroll:true,
            advanced:{ autoExpandHorizontalScroll:true }
        });
        
        $(".verScroll").mCustomScrollbar({
            set_width:"100%",
            scrollButtons:{ enable:true }
        });
    });

})(jQuery);