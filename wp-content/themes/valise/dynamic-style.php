<?php
/*	
*	---------------------------------------------------------------------
*	Defatch Dynamic Style
*	--------------------------------------------------------------------- 
*/

	header("Content-type: text/css;");
	$current_url = dirname(__FILE__);
	$wp_content_pos = strpos($current_url, 'wp-content');
	$wp_content = substr($current_url, 0, $wp_content_pos);
	require_once($wp_content . 'wp-load.php');
?>

		<?php /* General Color */ ?>
		<?php $site_pc = ot_get_option('valise_primary_color'); ?>
        
        .author:before, .pdate:before, .tags:before, 
        .testimonials .testi-details a, .project-slide-desc h3 a, .basic-desc h4, .pslide-desc h4,
        .blog-list-sidebar .continue, .blog-list-single .continue, .menu ul li.current-menu-ancestor a .menu-description i,
        .menu ul li:hover a .menu-description i,
        .welcome-block a, .project-list h3 a:hover,
        .blog-fullwidth .blog-details h5 a:hover, .blog-list .blog-details h5 a:hover, .blog-list-sidebar .blog-details h5 a:hover,
        .project-flexslider .flex-direction-nav li .flex-next, 
        .project-flexslider .flex-direction-nav li .flex-prev { color:<?php echo $site_pc; ?> !important; }
        
        .project-flexslider .flex-direction-nav li .flex-next, 
        .project-flexslider .flex-direction-nav li .flex-prev,
        .blogdetails .date { border-color: <?php echo $site_pc; ?> !important; }
               
        
        .home-project-flexslider .flex-control-paging li a.flex-active,
        .cat-list li.current-cat a:hover,
        a:hover#toTop,
        .project-block ul li h5,
        .testi-flexslider .flex-direction-nav li .flex-next:hover, 
        .testi-flexslider .flex-direction-nav li .flex-prev:hover,
        footer .widget-get-in-touch ul li a:hover,
        .next a:hover, .prev a:hover,
        a.visit:hover,
        .breadcrumbs,
        #top-bar-wrapper,
        .footer-main,
        .widget-get-in-touch ul li a,
        .latest-blog-entry .blog-entry-date span,
        .skillbar,
        .pricing-box-info,
        a.view-all,
        .pimage, .contentblock:hover .iconcircle, .cat-list li a,
        .page-numbers li a, .alignleft a:link, .alignleft a:visited, .alignright a:link, .alignright a:visited,
        span.current, .author-social li a, .divider a, .counter:hover .iconcircle, .counter .iconcircle,
        .post-navigation span, .post-navigation a:hover span,
        .tp-caption.valise_button { background-color:<?php echo $site_pc; ?> !important; color:#FFF !important; }
        
        .su-button { background-color:<?php echo $site_pc; ?>; }
        .testi-flexslider .flex-direction-nav li .flex-next, .testi-flexslider .flex-direction-nav li .flex-prev,
        .blog-flexslider .flex-direction-nav li .flex-next:hover, .blog-flexslider .flex-direction-nav li .flex-prev:hover, .comment-reply-link:hover,
        .team img:hover, .team .social-connect ul li a, .left-sidebar .widget_search input[type="submit"],
        .sf-menu li.sfHover ul a:hover,
        .su-button, input[type="submit"], .comment-respond input[type="submit"],
        .contact-form input[type="submit"], .nf-box, pre h2,
        .su-spoiler-title .spoiler-button { background-color:<?php echo $site_pc; ?> !important; }
        
        <?php $rgb = hex2rgb($site_pc); ?>
		.port-hover:hover .overlay {background-color:<?php echo $site_pc; ?> !important; background-color:rgba(<?php echo $rgb; ?>, 0.8) !important;}
        
        .sf-menu li:hover {border-bottom:3px solid <?php echo $site_pc; ?> !important;}
        
        .su-callout {border-left-color: <?php echo $site_pc; ?> !important;}
            
          
         <?php $site_sc = ot_get_option('valise_secondary_color'); ?>
         
         footer,
        footer .widget-get-in-touch ul li a,
        .widget-get-in-touch ul li a:hover,
        .hr-author, .author-social li a:hover, .author-block,
        .blog-flexslider .flex-direction-nav li .flex-next, .blog-flexslider .flex-direction-nav li .flex-prev, .team, .team .social-connect ul li a:hover,
        #today, .left-sidebar .widget_search input[type="submit"]:hover,
        .sf-menu li li a:link, .sf-menu li li a:visited, pre, .next a, .prev a, a.visit,
        .port-hover .overlay .zoom, .port-hover .overlay .details,
        .post-navigation a span, .post-navigation span:hover {background:<?php echo $site_sc; ?> !important;}
        
        .testimonial_list {background:url(<?php echo ot_get_option('valise_testimonial_bg'); ?>) repeat fixed <?php echo $site_sc; ?> !important;}
        
        .welcome-block a:hover,
        .project-block h2,
        .project-list h3 a,
        .project-flexslider .flex-direction-nav li .flex-next:hover, 
        .project-flexslider .flex-direction-nav li .flex-prev:hover, .sum,
        .count h2,
        .port-hover .overlay .zoom:hover, .port-hover .overlay .details:hover {
        	color:<?php echo $site_sc; ?> !important;
        }
        
        .project-flexslider .flex-direction-nav li .flex-next:hover, 
        .project-flexslider .flex-direction-nav li .flex-prev:hover {
        	border-color:<?php echo $site_sc; ?> !important;
        }

         <?php $site_mc = ot_get_option('valise_menu_text_color'); ?>
         .menu ul li a .menu-title, .menu ul li a .menu-description { color:<?php echo $site_mc; ?> !important; }
         
         
         <?php $site_hc = ot_get_option('valise_header_text_color'); ?>
         .welcome-block h2, .services-list h5, 
         .left-sidebar .widget h5, .comments h3, .comment-respond h3 { color:<?php echo $site_hc; ?> !important; }
         
         <?php $site_shc = ot_get_option('valise_sidebar_text_color'); ?>
         .left-sidebar .widget h5 { color:<?php echo $site_shc; ?> !important; }
         
         <?php $site_fhc = ot_get_option('valise_footer_header_color'); ?>
         .footer-widgets .widget h5 { color:<?php echo $site_fhc; ?> !important; }
          		

		body {
		<?php
		$google_body_font = ot_get_option('google_body_font');
		$body_font_family = $google_body_font['font-family'];
		$body_font_family = str_replace("+", " ", $body_font_family);
		echo 'font-family:'. $body_font_family .', sans-serif !important;';
		echo 'font-weight:'. $google_body_font['font-weight'] .' !important;'; 
		echo 'letter-spacing:'.$google_body_font['letter-spacing'] .' !important;'; 
		echo 'text-transform:'. $google_body_font['text-transform'] .' !important;'; 
		echo 'color:'. ot_get_option('content_text_color') .' !important;'; 
		echo 'font-size:'. ot_get_option('content_text_font_size') .' !important;'; 
		?>
		}
		
		input[type='submit']{
		font-family:<?php echo $body_font_family; ?>, sans-serif;
		}
		
		.menu ul li a .menu-title {
		<?php
		$google_menu_font = ot_get_option('google_menu_font');
		$menu_font_family = $google_menu_font['font-family'];
		$menu_font_family = str_replace("+", " ", $menu_font_family);
		echo 'font-family:'. $menu_font_family .', sans-serif !important;';
		echo 'font-weight:'. $google_menu_font['font-weight'] .' !important;'; 
		echo 'letter-spacing:'. $google_menu_font['letter-spacing'] .' !important;'; 
		echo 'text-transform:'. $google_menu_font['text-transform'] .' !important;'; 
		?>
		}
		
		h2.ptitle, .breadcrumbs {
		<?php
		$google_page_font = ot_get_option('google_page_font');
		$page_font_family = $google_page_font['font-family'];
		$page_font_family = str_replace("+", " ", $page_font_family);
		echo 'font-family:'. $page_font_family .', sans-serif !important;';
		echo 'font-weight:'. $google_page_font['font-weight'] .';'; 
		echo 'letter-spacing:'. $google_page_font['letter-spacing'] .' !important;'; 
		echo 'text-transform:'. $google_page_font['text-transform'] .' !important;'; 
		?>
		}
		
		.project-block h2,
        a.view-all, .blog-mask a,
        .services-list h5, .welcome-block h2, .contents h2, .testimonials,
		.blog-desc h5, .blog-desc h5 a,
       .featured-blog-details, .pdate, .tags,
        .project-block ul li h5, .project-list h3,
        .footer-widgets .widget h5,
        .copyright,
        .footer-menu ul li a,
        .footer-widgets .widget_categories ul li a, 
        .footer-widgets .widget-popular-posts ul li a,
        .footer-widgets .widget_archive ul li a,
        .footer-widgets .widget_meta ul li a,
        .comments h3, .comment-respond h3,
        .comment-details h6,
       	.comment-respond input[type='submit'], .contact-form input[type='submit'], .page-not-found a,
        .comment-details span,
        .left-sidebar .widget h5,
        .blog-list-sidebar .continue, .blog-list-single .continue,
        .page-not-found h4,.testimonials .testi-details span,
        .team h3 {
        <?php
		$google_fonts_headings = ot_get_option('google_fonts_headings');
		$headings_font_family = $google_fonts_headings['font-family'];
		$headings_font_family = str_replace("+", " ", $headings_font_family);
		echo 'font-family:'. $headings_font_family .', sans-serif !important;';
		echo 'font-weight:'. $google_fonts_headings['font-weight'] .' !important;'; 
		echo 'letter-spacing:'. $google_fonts_headings['letter-spacing'] .' !important;'; 
		echo 'text-transform:'. $google_fonts_headings['text-transform'] .' !important;'; 
		?>
		}
		
		.custom-font{
		<?php
		$google_custom_font = ot_get_option('google_custom_font');
		$custom_font_family = $google_custom_font['font-family'];
		$custom_font_family = str_replace("+", " ", $custom_font_family);
		echo 'font-family:'. $custom_font_family .', sans-serif;';
		echo 'font-weight:'. $google_custom_font['font-weight'] .';'; 
		echo 'letter-spacing:'. $google_custom_font['letter-spacing'] .';'; 
		echo 'text-transform:'. $google_custom_font['text-transform'] .';'; 
		?>
		}
						
		h1, h2, h3, h4, h5, h6, h1 a, h2 a, h3 a, h4 a, h5 a, h6 a, .su-service-title, .heading-wrapper h6, .su-spoiler-title {color:<?php echo ot_get_option('heading_color'); ?>;}
        
        h1, h2, h3, h4, h5, h6 {
        <?php
		$google_fonts_headings = ot_get_option('google_fonts_headings');
		$headings_font_family = $google_fonts_headings['font-family'];
		$headings_font_family = str_replace("+", " ", $headings_font_family);
		echo 'font-family:'. $headings_font_family .', sans-serif !important;';
		echo 'font-weight:'. $google_fonts_headings['font-weight'] .' !important;'; 
		echo 'letter-spacing:'. $google_fonts_headings['letter-spacing'] .' !important;'; 
		echo 'text-transform:'. $google_fonts_headings['text-transform'] .' !important;'; 
		?>
        }
		
		h1{font-size:<?php echo ot_get_option('h1_size'); ?> !important;}
		h2{font-size:<?php echo ot_get_option('h2_size'); ?> !important;}
		h3{font-size:<?php echo ot_get_option('h3_size'); ?> !important;}
		h4{font-size:<?php echo ot_get_option('h4_size'); ?> !important;}
		h5{font-size:<?php echo ot_get_option('h5_size'); ?> !important;}
		h6{font-size:<?php echo ot_get_option('h6_size'); ?> !important;}
				
		#title-wrapper{
		<?php 
		$title_bg = ot_get_option('title_background');
		echo 'background-color:'. $title_bg['background-color'] .';'; 
		echo 'background-image: url('. $title_bg['background-image'] .');'; 
		echo 'background-repeat:'. $title_bg['background-repeat'] .';'; 
		echo 'background-position:'. $title_bg['background-position'] .';'; 
		echo 'background-attachment:'. $title_bg['background-attachment'] .';'; 
		echo 'border-color:'. ot_get_option('title_area_border') .';'; 
		?>
		-webkit-background-size: <?php echo ot_get_option('title_area_background_size'); ?>;
		-moz-background-size: <?php echo ot_get_option('title_area_background_size'); ?>;
		-o-background-size: <?php echo ot_get_option('title_area_background_size'); ?>;
		background-size: <?php echo ot_get_option('title_area_background_size'); ?>;
		}
				
		.logo {padding-top:<?php echo ot_get_option('logo_padding_top'); ?> !important; margin-bottom:<?php echo ot_get_option('logo_margin_bottom'); ?>;}
		
		.menu ul li a .menu-title {font-size:<?php echo ot_get_option('menu_font_size'); ?> !important;}
		
		#top-bar-wrapper #top-bar, #top-bar-wrapper #top-bar a{color:<?php echo ot_get_option('top_bar_text_color', '#ffffff'); ?>;}
        
        
        <?php if(ot_get_option('port_slide_icons')!="") { ?>
			.flex-direction-nav { display:none !important; }
		<?php } ?>
        
		<?php /* Custom CSS from panel */ ?>
		<?php echo ot_get_option('custom_css'); ?>