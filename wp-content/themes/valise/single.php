<?php get_header(); ?>

        <!--BREADCRUMB / TITLE-->
        <section id="title-wrapper" class="title-breadcrumb">
            <div class="inside">
           
                 <?php
					if( get_post_meta( $post->ID, "pages_title", true ) != "") { 
						?><h2 class="ptitle"><?php echo stripslashes(get_post_meta( $post->ID, "pages_title", true )); ?></h2><?php 
					} elseif (post_password_required()) {
						echo '<h2 class="ptitle">Protected</h2>';
					} else {
						?><h2 class="ptitle"><?php the_title(); ?></h2><?php
					}
				?> 
                <?php valise_breadcrumb(); ?>
            </div>
        </section>

	<!--BLOG SIDEBAR-->
	<section class="blog-page">
		<div class="inside clear">

			<!--LEFT CONTAINER-->
			<div class="right-content <?php if(ot_get_option('valise_blog_sidebar')=="Right") { echo "left"; } ?>">
				<div class="clear">
					<?php
						if ( post_password_required() ) {
							?><div class="password-protected"><?php the_content(); ?></div><?php
						}else {
							while (have_posts()) : the_post(); 						
								?>
								<div class="blog-list-single">
                                
									<div class="bimage">
										<?php
											$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'blog-large');

											if ( ( function_exists( 'get_post_format' ) && 'image' == get_post_format( $post->ID ) )  ) {
												if ( has_post_thumbnail() ) {
													the_post_thumbnail('blog-large');
												}
											}elseif ( ( function_exists( 'get_post_format' ) && 'video' == get_post_format( $post->ID ) )  ) { 
												if(get_post_meta( $post->ID, "post_video_type", true ) == "self") {
													?>
													<video id="videojs_blog" class="video-js vjs-default-skin" controls preload="none" width="770" height="438" poster="<?php echo $large_image_url[0]; ?>" data-setup="{}">
														<source src="<?php echo get_post_meta( $post->ID, "post_video_url", true ); ?>" type='video/mp4' />
													    <source src="<?php echo get_post_meta( $post->ID, "post_video_url", true ); ?>" type='video/webm' />
													    <source src="<?php echo get_post_meta( $post->ID, "post_video_url", true ); ?>" type='video/ogg' />
													</video>													
													<?php
												} if(get_post_meta( $post->ID, "post_video_type", true ) == "youtube") {
													?>
													<iframe width="770" height="438" src="http://www.youtube.com/embed/<?php echo get_post_meta( $post->ID, "post_video_url", true ); ?>?autohide=1&amp;iv_load_policy=3&amp;modestbranding=1&amp;rel=0&amp;showinfo=0" allowfullscreen></iframe>													
													<?php
												} if(get_post_meta( $post->ID, "post_video_type", true ) == "vimeo") {
													?>
													<iframe src="http://player.vimeo.com/video/<?php echo get_post_meta( $post->ID, "post_video_url", true ); ?>" width="770" height="438" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>							
													<?php
												} if(get_post_meta( $post->ID, "post_video_type", true ) == "dailymotion") {
													?>
                                                    <iframe width="770" height="438" src="http://www.dailymotion.com/embed/video/<?php echo get_post_meta( $post->ID, "post_video_url", true ); ?>"></iframe>				
													<?php
												} 
											}elseif ( ( function_exists( 'get_post_format' ) && 'gallery' == get_post_format( $post->ID ) )  ) { 
												?>
												<div class="blog-flexslider">
													<ul class="slides">
														<?php
															$args = array( 'post_type' => 'attachment', 'orderby' => 'menu_order', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $post->ID );

															$attachments = get_posts( $args );
															if ( $attachments ) {								
																foreach ( $attachments as $attachment ) {
																	$attachment_id = $attachment->ID;
																	$type = get_post_mime_type( $attachment->ID ); 

																	switch ( $type ) {
																		case 'video/mp4':
																			?>
																			<li>
																				<video id="videojs_gallery" class="video-js vjs-default-skin" controls preload="none" width="770" height="438" poster="" data-setup="{}">
																					<source src="<?php echo wp_get_attachment_url( $attachment->ID ); ?>" type='video/mp4' />
																				</video>
																			</li>
																			<?php
																		break;
																		default:
																			?>
																			<li><?php echo wp_get_attachment_image( $attachment->ID, 'blog-large' ); ?></li>
																			<?php
																		break;
																	}
																}
															}			
														?>
													</ul>
												</div> 
												<?php
											}
										?>	
									</div>
                                    
                                    <div <?php post_class('single-desc'); ?>>
                                    
                                        <h2><?php the_title(); ?></h2>
                                    
                                        <label class="clear blog-detail">		
                                          <span><i class="icon-calendar"></i> <?php the_time(get_option('date_format')); ?></span>
                                          <span><i class="icon-user"></i> <?php the_author_posts_link(); ?></span>
                                          <?php if (has_tag()) { ?><span><i class="icon-tag"></i> <?php the_tags('',', ',''); ?></span><?php } ?>
                                          <?php if (has_category()) { ?><span><i class="icon-briefcase"></i> <?php the_category(', '); ?></span><?php } ?>
                                        </label>
                                                                                
                                        <div class="divider-arrow-down"></div> 
                                                                               
										<?php the_content(); ?>
                                        
                                        <?php wp_link_pages('before=<p class="post-navigation">&<span>after=</span></p>&pagelink=<span>%</span>'); ?>
                                        
									</div>
                                   
								</div>
								<?php
							endwhile;

							if(ot_get_option('valise_hide_author')=="") {
								?>
									<!--AUTHOR-->
									<div class="author-block clear">
                                    	<h3><?php _e( 'About the author', CSB_THEME_NAME ); ?> / <?php the_author_meta( 'display_name' ); ?></h3>
										<?php echo get_avatar( get_the_author_meta( 'ID' ), 80 ); ?>
                                        <ul class="clear author-social">
											<?php if (get_the_author_meta('email')) {
											?>
                                           	<li><a href="mailto:<?php the_author_meta('email'); ?>" target="_blank"><i class="icon-envelope icon-large"></i></a></li>
											<?php } else { echo ''; }?>
                                            
                                            <?php if (get_the_author_meta('user_url')) {
											?>
                                           	<li><a href="<?php the_author_meta('user_url'); ?>" target="_blank"><i class="icon-link icon-large"></i></a></li>
											<?php } else { echo ''; }?>
                                            
                                            <?php if (get_the_author_meta('facebook')) {
											?>
                                            <li><a href="<?php the_author_meta('facebook'); ?>" target="_blank"><i class="icon-facebook icon-large"></i></a></li>
                                            <?php } else { echo ''; }?>
                                            
                                            <?php if (get_the_author_meta('facebook')) {
											?>
                                            <li><a href="<?php the_author_meta('twitter'); ?>" target="_blank"><i class="icon-twitter icon-large"></i></a></li>
                                            <?php } else { echo ''; }?>
                                            
                                        </ul>
										<p><?php the_author_meta( 'user_description' ); ?></p>
									</div>
								<?php
							}

							comments_template();
						}			
					?>
				</div>	

			</div>

			<!--RIGHT SIDEBAR-->
			<div class="left-sidebar <?php if(ot_get_option('valise_blog_sidebar')=="Right") { echo "right"; } ?>">
				<?php
					if($sidebar_choice == "Default") { get_sidebar( 'blog' ); } else { valise_get_custom_sidebar(); }
				?>
			</div>
		</div>
	</section>

<?php get_footer(); ?>