<?php get_header(get_post_meta($post->ID, 'header_choice_select', true)); ?>
    
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "revolution-slider") { 
		}
	?>
    
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "notitle") { 
		echo '<hr>';
		}
	?>
    
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "") { 
	?>
        <!--BREADCRUMB / TITLE-->
        <section id="title-wrapper" class="title-breadcrumb">
            <div class="inside">
                <h2 class="ptitle">
                    <?php the_title(); ?>
                </h2>
                <?php valise_breadcrumb(); ?>
            </div>
        </section>
	<?php
		}
	?>
	  

	<!--PORTFOLIO SINGLE POST-->
	<section class="project-page">
		<div class="inside">

			<?php
				while (have_posts()) : the_post(); 
					$pattachment_order = ot_get_option('valise_attachment_order');
					$pattachment_orderby = ot_get_option('valise_attachment_orderby');
					
					if(ot_get_option('valise_portfolio_thumb_slider')=="") {
						$args = array( 'post_type' => 'attachment', 'numberposts' => -1, 'order' => $pattachment_order, 'orderby' => $pattachment_orderby, 'post_status' => null, 'post_parent' => $post->ID );
					}else {
						$thumb_id = get_post_thumbnail_id( get_the_ID() );
						$args = array( 'post_type' => 'attachment', 'numberposts' => -1, 'order' => $pattachment_order, 'orderby' => $pattachment_orderby, 'post_status' => null, 'post_parent' => $post->ID, 'exclude' => $thumb_id );
					}					
					$attachments = get_posts( $args );		

					//SLIDER STYLE
					if( get_post_meta( $post->ID, "image_style", true ) == "work_slider") {
						?>
							<!--SLIDER-->
							<div class="portfolio-slider">
								<div class="project-flexslider">
									<ul class="slides">
										
										<?php $items = get_post_meta( $post->ID, "portfolio_images_videos", true );
											
											if ($items) {
											
											foreach($items as $item) : 
											
												if ($item['image']) {
										?>
										
											<li><a href="<?php echo $item['image-link'] ; ?>" target="_blank"><img src="<?php echo $item['image'] ; ?>" title="<?php echo $item['img-title'] ; ?>" /></a></li>
										
										<?php } else { ?>
											
											<li><?php echo $item['video-frame'] ; ?></li>
										
										<?php } endforeach; } ?>
										
									</ul>
								</div>

								<!--DESCRIPTION-->
								<div class="clear">
									<div class="pslide-desc">
										<h2><?php the_title(); ?></h2>
										<?php the_content(); ?>
									</div>
									<div class="pinfo">
                                        
                                            <ul class="project-detail">		
											   <?php if(get_post_meta( $post->ID, "launch_date", true)) { ?>
                                               <li><i class="icon-calendar"></i><?php echo get_post_meta( $post->ID, "launch_date", true ); ?>	</li>
                                               <?php } ?>
                                               <?php if(get_post_meta( $post->ID, "developed_by_using", true)) { ?>
                                                <li><i class="icon-beaker"></i><?php echo get_post_meta( $post->ID, "developed_by_using", true ); ?></li>
                                                <?php } ?>
                                                <li><i class="icon-briefcase"></i><?php echo get_the_term_list( $post->ID, 'portfolio_categories', ' ', ', ', '' ); ?></li>
                                            </ul>
                                                
                                            <?php if(get_post_meta( $post->ID, "valise_vsite", true)) { ?>
                                                    <a href="<?php echo get_post_meta( $post->ID, "valise_vsite", true ); ?>" class="visit" target="_blank"><i class="icon-external-link"></i> &nbsp;
                                                    
                                                        <?php if(get_post_meta( $post->ID, "valise_vsite_lable", true )) { echo get_post_meta( $post->ID, "valise_vsite_lable", true ); } else { _e( 'View Site', CSB_THEME_NAME ); } ?>
                                                    </a>
                                            <?php } ?>
                                    
                                        
										<div class="post-link clear">
                                            <span class="prev"><?php previous_post_link('%link', '<i class="icon-chevron-left" title="Previous"></i>'); ?></span>
                                            <span class="next"><?php next_post_link('%link', '<i class="icon-chevron-right" title="Next"></i>'); ?></span>
                                        </div>
									</div>
								</div>
							</div>
						<?php

						//DEFAULT STYLE
					}else {							
						?>
							<div class="portfolio-basic clear">
                            
                            	<!--ATTACHMENTS-->
								<div class="basic-attachment">
									<ul>
										
										<?php $items = get_post_meta( $post->ID, "portfolio_images_videos", true );
											
											if ($items) {
											
											foreach($items as $item) : 
											
												if ($item['image']) {
										?>
										
											<li>
												<?php if($item['image-link']) { ?><a href="<?php echo $item['image-link'] ; ?>" target="_blank"><?php } ?>
													<img src="<?php echo $item['image'] ; ?>" title="<?php echo $item['img-title'] ; ?>" style="width:100%;"/>
												<?php if($item['image-link']) { ?></a><?php } ?>
											</li>
										
										<?php } else { ?>
											
											<li style="width:100%;"><?php echo $item['video-frame'] ; ?></li>
										
										<?php } endforeach; } ?>                                        
                                         
									</ul>
								</div>
                                
								<!--DESCRIPTION-->
								<div class="basic-desc">
                                
                                	
                                    
									<h2><?php the_title(); ?></h2>
                                    
                                    <p><?php the_content(); ?></p>
                                    
									<ul class="project-detail">		
                                       <?php if(get_post_meta( $post->ID, "launch_date", true)) { ?>
                                       <li><i class="icon-calendar"></i><?php echo get_post_meta( $post->ID, "launch_date", true ); ?>	</li>
                                       <?php } ?>
                                       <?php if(get_post_meta( $post->ID, "developed_by_using", true)) { ?>
                                        <li><i class="icon-beaker"></i><?php echo get_post_meta( $post->ID, "developed_by_using", true ); ?></li>
                                        <?php } ?>
                                        <li><i class="icon-briefcase"></i><?php echo get_the_term_list( $post->ID, 'portfolio_categories', ' ', ', ', '' ); ?></li>
                                    </ul>
										
                                    <?php if(get_post_meta( $post->ID, "valise_vsite", true)) { ?>
											<a href="<?php echo get_post_meta( $post->ID, "valise_vsite", true ); ?>" class="visit" target="_blank"><i class="icon-external-link"></i> &nbsp;
											
												<?php if(get_post_meta( $post->ID, "valise_vsite_lable", true )) { echo get_post_meta( $post->ID, "valise_vsite_lable", true ); } else { _e( 'View Site', CSB_THEME_NAME ); } ?>
											</a>
									<?php } ?>
                                    
                                    <div class="post-link clear">
										<span class="prev"><?php previous_post_link('%link', '<i class="icon-chevron-left" title="Previous"></i>'); ?></span>
										<span class="next"><?php next_post_link('%link', '<i class="icon-chevron-right" title="Next"></i>'); ?></span>
									</div>
                                    
									
								</div>
                                								
							</div>
						<?php						
					}
				endwhile;
			?>	

		</div>
	</section>

<?php get_footer(); ?>