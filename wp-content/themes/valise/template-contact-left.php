<?php
/*
Template Name: Contact Left Sidebar
*/
?>

<?php get_header(get_post_meta($post->ID, 'header_choice_select', true)); ?>
    
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "revolution-slider") { 
		}
	?>
    
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "notitle") { 
		echo '<hr>';
		}
	?>
    
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "") { 
	?>
        <!--BREADCRUMB / TITLE-->
        <section id="title-wrapper" class="title-breadcrumb">
            <div class="inside">
                <?php 
                    valise_get_page_title(); 
                    valise_breadcrumb(); 
                ?>
            </div>
        </section>
	<?php
		}
	?>

	<section class="theme-pages">
		<div class="inside clear">

			<!--LEFT CONTAINER-->
			<div class="right-content">
				<?php
					if (have_posts()) : 
						while (have_posts()) : the_post(); 
							the_content(); 								
						endwhile; 
					endif;
				?>
			</div>

			<!--RIGHT SIDEBAR-->
			<div class="left-sidebar">
				<?php
					if($sidebar_choice == "Default") { get_sidebar('contact'); } else { valise_get_custom_sidebar(); }
				?>
			</div>

		</div>
	</section>

<?php get_footer(); ?>