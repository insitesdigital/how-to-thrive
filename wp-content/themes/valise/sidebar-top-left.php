	<div id="topleft-widget-area" class="widget-area size-wrap">
		<?php
            if ( is_active_sidebar( 'top_left_sidebar' ) ) : dynamic_sidebar( 'top_left_sidebar' );
            else : _e( 'This is topleft widget area, so please add widgets here...', CSB_THEME_NAME );
            endif;
        ?>
	</div>
