<?php
/*
Template Name: Homepage
*/
?>

<?php get_header(get_post_meta($post->ID, 'header_choice_select', true)); ?>

	
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "revolution-slider") { 
			?><?php 
		}else {
		}
	?>
    <?php
		valise_welcome();
		valise_services_list();
	?>

	<section class="contents">
			<div class="testimonial_list">
				<div class="inside clear"><?php valise_testimonial_list(); ?></div>
            </div>
        <div class="inside clear">
			<div class=""><?php valise_blog_list(); ?></div>
		</div>
	</section>

<?php get_footer(); ?>