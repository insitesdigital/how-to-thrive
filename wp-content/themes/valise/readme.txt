= VALISE WORDPRESS THEME =

<p><strong>2014 January 10 � Version 1.1</strong>
</p>

<pre>

Replace all following files.
styling issue fixed :
- style.css
- responsive.css
- dynamic-style.php
- includes/shortcodes/css/styles.css

Shortcode (Show Posts) issue fixed :
- includes/shortcodes/lib/available.php
- includes/shortcodes/lib/shortcodes.php

Custom sidebar issue fixed :
- single.php
- template-blog-fullwidth.php
- template-blog-grid-sidebar.php
- template-blog-list-sidebar.php

Features Added :
- includes/theme-options/assets/meta-boxes.php
- includes/theme-options/assets/theme-options.php

</pre>

* by Defatch, http://defatch.com