<?php
/**
 * Initialize the meta boxes. 
 */
add_action( 'admin_init', '_custom_meta_boxes' );

function _custom_meta_boxes() {
  $header_opt_meta_box = array(
    'id'          => 'header_opt_meta_box',
    'title'       => 'Header element options',
    'desc'        => '',
    'pages'       => array( 'page', 'portfolio' ),
    'context'     => 'side',
    'priority'    => 'high',
    'fields'      => array(
	  array(
        'id'          => 'header_choice_select',
        'label'       => 'Select header element:',
        'desc'        => '',
        'std'         => '',
        'type'        => 'select',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => '',
            'label'       => 'Title',
            'src'         => ''
          ),
          array(
            'value'       => 'notitle',
            'label'       => 'No Title',
            'src'         => ''
          ),
          array(
            'value'       => 'revolution-slider',
            'label'       => 'Revolution Slider',
            'src'         => ''
          )
        ),
      ),
	  array(
        'id'          => 'header_opt_info',
        'label'       => 'Header Element info',
        'desc'        => 'For Blog page header choice go to: Appearance / Theme Options / Blog Options',
        'std'         => '',
        'type'        => 'textblock',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      )
  	)
  );
  
ot_register_meta_box( $header_opt_meta_box );
	
$advanced_opt_meta_box = array(
    'id'          => 'advanced_opt_meta_box',
    'title'       => 'Revolution Slider',
    'desc'        => '',
    'pages'       => array( 'page', 'portfolio' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
      array(
        'id'          => 'rev_slider_custom',
        'label'       => 'Revolution Slider',
        'desc'        => 'Paste your slider alias (eg: valise)',
        'std'         => '',
        'type'        => 'text',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      )
  	)
  );
  
ot_register_meta_box( $advanced_opt_meta_box );

$services_meta_box = array(
    'id'          => 'services_meta_box',
    'title'       => 'Services Options',
    'desc'        => '',
    'pages'       => array( 'services' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
      array(
        'id'          => 'services_button_text',
        'label'       => 'Services Button Text',
        'desc'        => 'Enter your services box button text here',
        'std'         => '',
        'type'        => 'text',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	   array(
        'id'          => 'services_button_link',
        'label'       => 'Services Button Link',
        'desc'        => 'Enter your services box button link here',
        'std'         => '',
        'type'        => 'text',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	   array(
        'id'          => 'services_icon',
        'label'       => 'Services Icon',
        'desc'        => 'Select your service icon from <a href="http://www.defatch.com/themes/valise/style/icons/" target="_blank">here</a>',
        'std'         => 'icon-heart',
        'type'        => 'text',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      )
  	)
  );
  
ot_register_meta_box( $services_meta_box );


$testi_meta_box = array(
    'id'          => 'testi_meta_box',
    'title'       => 'Testimonial Options',
    'desc'        => '',
    'pages'       => array( 'testimonials' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
	   array(
        'id'          => 'testimonial_client_img',
        'label'       => 'Client Image',
        'desc'        => 'Upload your client image recommended size 130x130',
        'std'         => '',
        'type'        => 'upload',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'testimonial_company',
        'label'       => 'Company Name',
        'desc'        => 'Add your client company',
        'std'         => '',
        'type'        => 'text',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      )
  	)
  );
  
ot_register_meta_box( $testi_meta_box );


$page_meta_box = array(
    'id'          => 'page_meta_box',
    'title'       => 'Page Options',
    'desc'        => '',
    'pages'       => array( 'page', 'post' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
	   array(
        'id'          => 'pages_title',
        'label'       => 'Page Title',
        'desc'        => 'Add your page title here if you want another title header',
        'std'         => '',
        'type'        => 'text',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	   array(
        'id'          => 'page_bg_image',
        'label'       => 'Page Background',
        'desc'        => 'Add your background image here',
        'std'         => '',
        'type'        => 'upload',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      )
  	)
  );
  
ot_register_meta_box( $page_meta_box );

$post_meta_box = array(
    'id'          => 'post_meta_box',
    'title'       => 'Video Options',
    'desc'        => '',
    'pages'       => array( 'post' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
	   
	  array(
        'id'          => 'post_video_type',
        'label'       => 'Video Host',
        'desc'        => 'Add your video url here',
        'std'         => '',
        'type'        => 'select',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'youtube',
            'label'       => 'YouTube',
            'src'         => ''
          ),
          array(
            'value'       => 'vimeo',
            'label'       => 'Vimeo',
            'src'         => ''
          ),
		  array(
            'value'       => 'dailymotion',
            'label'       => 'Daily Motion',
            'src'         => ''
          ),
          array(
            'value'       => 'self',
            'label'       => 'Self Host',
            'src'         => ''
          )
        )
		),
	  array(
        'id'          => 'post_video_url',
        'label'       => 'Post Video URL',
        'desc'        => 'Self Host : Video URL<br /> YouTube : Video Id (Eg: Ycv5fNd4AeM)<br /> Vimeo : Vimeo Id (Eg: 50522981)<br /> Daily Motion : Daily Motion Id (Eg: x17x5r6)',
        'std'         => '',
        'type'        => 'text',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      )
  	)
  );
  
ot_register_meta_box( $post_meta_box );

$port_item_meta_box = array(
    'id'          => 'port_item_meta_box',
    'title'       => 'Add Portfolio item',
    'desc'        => '',
    'pages'       => array( 'portfolio' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
	   
	   array(
        'id'          => 'portfolio_images_videos',
        'label'       => 'Add Portfolio Items',
        'desc'        => 'Add Images or Videos iframe. Click "Add New"',
        'std'         => '',
        'type'        => 'list-item',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
		'settings'    => array(
				array(
					'id'          => 'image_heading_title',
					'label'       => 'For Image',
					'desc'        => '<h1>For Image</h1>',
					'type'        => 'textblock'
				),
				array( 
					'id'          => 'img-title',
					'label'       => 'Image Title',
					'type'        => 'text',
				),
				array( 
					'id'          => 'image',
					'label'       => 'Image',
					'type'        => 'upload',
				),
				array(
					'id'          => 'image-link',
					'label'       => 'Link',
					'type'        => 'text',
				),
				array(
					'id'          => 'video_heading_title',
					'label'       => 'For Video',
					'desc'        => '<h1>For Video</h1>',
					'type'        => 'textblock'
				),
				array(
					'id'          => 'video-frame',
					'label'       => 'Video Iframe',
					'type'        => 'textarea-simple',
					'rows'		  => '3',
				),
			),
      ),
	  
  	)
  );
  
ot_register_meta_box( $port_item_meta_box );

$portfolio_post_opt_meta_box = array(
    'id'          => 'portfolio_post_opt_meta_box',
    'title'       => 'Item Information',
    'desc'        => '',
    'pages'       => array( 'portfolio' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
	
	array(
        'id'          => 'pro_short_title',
        'label'       => 'Project Short Title',
        'desc'        => 'Eg : Short title about your project',
        'std'         => '',
        'type'        => 'text',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	array(
        'id'          => 'thumb_size',
        'label'       => 'Thumbnail Size',
        'desc'        => 'NOTE : This resize option only apply for portfolio mansory page template. This is for featured image view size.',
        'std'         => 'medium',
        'type'        => 'select',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'thumb_large',
            'label'       => 'Large (581x297)',
            'src'         => ''
          ),
		  array(
            'value'       => 'thumb_medium',
            'label'       => 'Medium (288x147)',
            'src'         => ''
          ),
          array(
            'value'       => 'thumb_verticle',
            'label'       => 'Vertical (288x297)',
            'src'         => ''
          ),
		  array(
            'value'       => 'thumb_horizontal',
            'label'       => 'Horizontal (581x147)',
            'src'         => ''
          )
        ),
      ),
	
	array(
        'id'          => 'image_style',
        'label'       => 'Page Style',
        'desc'        => 'Select your portfolio single page style',
        'std'         => '',
        'type'        => 'select',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'work_default',
            'label'       => 'Style One',
            'src'         => ''
          ),
		  array(
            'value'       => 'work_slider',
            'label'       => 'Style Two',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'launch_date',
        'label'       => 'Launch Date',
        'desc'        => 'Eg : 01 Dec 2013',
        'std'         => '',
        'type'        => 'text',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'developed_by_using',
        'label'       => 'Developed By Using',
        'desc'        => 'Eg : HTML, JAVA, CSS',
        'std'         => '',
        'type'        => 'text',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_vsite',
        'label'       => 'Project Link',
        'desc'        => 'Eg : http://google.com',
        'std'         => '',
        'type'        => 'text',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_vsite_lable',
        'label'       => 'Launch Project Button Text',
        'desc'        => 'Eg : Launch Project',
        'std'         => '',
        'type'        => 'text',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      )
	  
	)
  );
  
ot_register_meta_box( $portfolio_post_opt_meta_box );
}