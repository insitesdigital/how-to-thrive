<?php
/**
 * Initialize the custom theme options.
 */
add_action( 'admin_init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( 'option_tree_settings', array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array( 
    'contextual_help' => array( 
      'sidebar'       => ''
    ),
    'sections'        => array( 
      array(
        'id'          => 'general_default',
        'title'       => 'General options'
      ),
      array(
        'id'          => 'header_area_heading',
        'title'       => 'Header options'
      ),
      array(
        'id'          => 'background_options',
        'title'       => 'Background options'
      ),
      array(
        'id'          => 'color_options',
        'title'       => 'Color options'
      ),
      array(
        'id'          => 'typography_heading',
        'title'       => 'Typography'
      ),
      array(
        'id'          => 'footer_options',
        'title'       => 'Footer options'
      ),
      array(
        'id'          => 'social_media',
        'title'       => 'Social Media'
      ),
      array(
        'id'          => 'cpt',
        'title'       => 'Custom Post Types'
      ),
      array(
        'id'          => 'translation',
        'title'       => 'Translation'
      ),
      array(
        'id'          => 'css',
        'title'       => 'Custom CSS'
      ),
      array(
        'id'          => 'home_page',
        'title'       => 'Home Page'
      ),
      array(
        'id'          => 'blog_page',
        'title'       => 'Blog Page'
      ),
      array(
        'id'          => 'portfolio_page',
        'title'       => 'Portfolio Page'
      )
    ),
    'settings'        => array( 
      array(
        'id'          => 'theme_layout',
        'label'       => 'Layout',
        'desc'        => 'This setting will apply selected layout to your website. Click on image and Save Changes.',
        'std'         => 'full-width',
        'type'        => 'radio-image',
        'section'     => 'general_default',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      
	  array(
        'id'          => 'valise_pattern',
        'label'       => 'Pattern Type',
        'desc'        => 'Select your background patter type, when its boxed layout.',
        'std'         => '',
        'type'        => 'select',
        'section'     => 'general_default',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'img/patterns/none.png',
            'label'       => 'Select Pattern',
            'src'         => ''
          ),
          array(
            'value'       => 'img/patterns/gray_lines.png',
            'label'       => 'Gray Lines',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/noise_lines.png',
            'label'       => 'Noise Lines',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/tiny_grid.png',
            'label'       => 'Tiny Grid',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/strange_bullseyes.png',
            'label'       => 'Bullseye',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/gray_paper.png',
            'label'       => 'Gray Paper',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/norwegian_rose.png',
            'label'       => 'Norwegian Rose',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/subtlenet.png',
            'label'       => 'Subtle Net',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/polyester_lite.png',
            'label'       => 'Polyester Lite',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/absurdidad.png',
            'label'       => 'Absurdity',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/white_bed_sheet.png',
            'label'       => 'White Bed Sheet',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/subtle_stripes.png',
            'label'       => 'Subtle Stripes',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/lghtmesh.png',
            'label'       => 'Light Mesh',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/rough_diagonal.png',
            'label'       => 'Rough Diagonal',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/arab_tile.png',
            'label'       => 'Arabesque',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/stacked_circles.png',
            'label'       => 'Stack Circles',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/hexellence.png',
            'label'       => 'Hexellence',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/white_texture.png',
            'label'       => 'White Texture',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/concrete_wall.png',
            'label'       => 'Concrete Wall',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/brushed_alu.png',
            'label'       => 'Brush Aluminum',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/groovepaper.png',
            'label'       => 'Groovepaper',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/diagonal_noise.png',
            'label'       => 'Diagonal Noise',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/rockywall.png',
            'label'       => 'Rocky Wall',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/whitey.png',
            'label'       => 'Whitey',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/bright_squares.png',
            'label'       => 'Bright Squares',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/freckles.png',
            'label'       => 'Freckles',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/wallpaper.png',
            'label'       => 'Wallpaper',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/project_papper.png',
            'label'       => 'Project Paper',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/cubes.png',
            'label'       => 'Cubes',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/washi.png',
            'label'       => 'Washi',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/dotnoise.png',
            'label'       => 'Dot Noise',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/xv.png',
            'label'       => 'xv',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/plaid.png',
            'label'       => 'Little Plaid',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/old_wall.png',
            'label'       => 'Old Wall',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/connect.png',
            'label'       => 'Connect',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/ravenna.png',
            'label'       => 'Ravenna',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/smooth_wall.png',
            'label'       => 'Smooth Wall',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/tapestry.png',
            'label'       => 'Tapestry',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/psychedelic.png',
            'label'       => 'Psychedelic',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/scribble_light.png',
            'label'       => 'Scribble Light',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/gplay.png',
            'label'       => 'GPlay',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/lil_fiber.png',
            'label'       => 'Lil Fiber',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/first_aid.png',
            'label'       => 'First Aid',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/frenchstucco.png',
            'label'       => 'Frenchstucco',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/light_wool.png',
            'label'       => 'Light Wool',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/gradient_squares.png',
            'label'       => 'Gradient Squares',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/escheresque.png',
            'label'       => 'Escheresque',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/climpek.png',
            'label'       => 'Climpek',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/lyonnette.png',
            'label'       => 'Lyonnette',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/greyfloral.png',
            'label'       => 'Gray Floral',
            'src'         => ''
          ),
		  array(
            'value'       => 'img/patterns/reticular_tissue.png',
            'label'       => 'Reticular Tissue',
            'src'         => ''
          )
        ),
      ),
	  array(
        'id'          => 'valise_site_layout',
        'label'       => 'Responsive layout',
        'desc'        => 'Enable responsiveness mode for your website.',
        'std'         => '',
        'type'        => 'checkbox',
        'section'     => 'general_default',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
		'choices'     => array( 
          array(
            'value'       => 'enable_responsive',
            'label'       => 'Enable Responsive',
            'src'         => ''
          )
        ),
      ),
	  array(
        'id'          => 'valise_page_comments',
        'label'       => 'Enable page comments',
        'desc'        => 'Enable page comments.',
        'std'         => '',
        'type'        => 'checkbox',
        'section'     => 'general_default',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
		'choices'     => array( 
          array(
            'value'       => 'enable_page_comments',
            'label'       => 'Enable Page Comments',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'valise_favicon',
        'label'       => 'Custom favicon',
        'desc'        => 'Add custom favicon (16px x 16px)',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'general_default',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'iphone_icon',
        'label'       => 'Apple iPhone icon',
        'desc'        => 'Icon for Apple iPhone (57px x 57px). This icon is used for Bookmark on Home screen.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'general_default',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'iphone_retina_icon',
        'label'       => 'Apple iPhone retina icon',
        'desc'        => 'Icon for Apple iPhone retina (114px x114px). This icon is used for Bookmark on Home screen.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'general_default',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'ipad_icon',
        'label'       => 'Apple iPad icon',
        'desc'        => 'Icon for Apple iPad (72px x 72px). This icon is used for Bookmark on Home screen.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'general_default',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'ipad_retina_icon',
        'label'       => 'Apple iPad retina icon',
        'desc'        => 'Icon for Apple iPad retina (144px x 144px). This icon is used for Bookmark on Home screen.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'general_default',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'login_logo',
        'label'       => 'Custom WP admin login logo',
        'desc'        => 'Upload a custom logo for Wordpress login page.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'general_default',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'analytics_code',
        'label'       => 'Analytics code',
        'desc'        => 'Paste your Google Analytics or other tracking code in text area.',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'general_default',
        'rows'        => '6',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'valise_meta_descriptions',
        'label'       => 'Descriptions',
        'desc'        => 'Enter your meta descriptions',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'header_area_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'valise_meta_keywords',
        'label'       => 'Keywords',
        'desc'        => 'Enter your meta keywords',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'header_area_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),

	array(
        'id'          => 'disable_breadcrumbs',
        'label'       => 'Breadcrumbs',
        'desc'        => 'Disables breadcrumbs from page title area.',
        'std'         => '',
        'type'        => 'checkbox',
        'section'     => 'header_area_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'disable breadcrumbs',
            'label'       => 'Disable breadcrumbs',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'top_bar_title',
        'label'       => 'Top bar',
        'desc'        => '<h1>Top bar</h1>',
        'std'         => '',
        'type'        => 'textblock',
        'section'     => 'header_area_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'top_bar',
        'label'       => 'Show top bar?',
        'desc'        => 'This option enables top bar above the menu header. If it is activated, new widget areas appear in Appearance/Widgets - \'Top Bar Sidebar Left\' and \'Top Bar Sidebar Right\'.',
        'std'         => '',
        'type'        => 'checkbox',
        'section'     => 'header_area_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'enable_top_bar',
            'label'       => 'Enable top bar',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'top_bar_text_color',
        'label'       => 'Top bar text color',
        'desc'        => 'Click input field for colorpicker or enter your custom value',
        'std'         => '#ffffff',
        'type'        => 'colorpicker',
        'section'     => 'header_area_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'logo_options_title',
        'label'       => 'Logo options title',
        'desc'        => '<h1>Logo options</h1>',
        'std'         => '',
        'type'        => 'textblock',
        'section'     => 'header_area_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'logo_upload',
        'label'       => 'Logo upload',
        'desc'        => 'Click Upload button to insert the image. Make sure to press File URL in link field before pressing Send to OptionTree. You may also directly paste background image url into the input field.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'header_area_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'retina_logo_upload',
        'label'       => 'Retina logo upload',
        'desc'        => 'Retina logo should be 2x the size of default logo keeping the aspect ratio!',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'header_area_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'retina_logo_width',
        'label'       => 'Standart logo width (fill only, if retina logo is present)',
        'desc'        => 'Enter your standard (NOT RETINA) logo width.<br /> Remember to add px value in the end. Example: 100px',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'header_area_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'retina_logo_height',
        'label'       => 'Standart logo height (fill only, if retina logo is present)',
        'desc'        => 'Enter your standard (NOT RETINA) logo height.<br /> Remember to add px value in the end. Example: 30px',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'header_area_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'logo_padding_top',
        'label'       => 'Logo Padding Top',
        'desc'        => 'Logo margin from left. Remember to add px value after the number. Example: 10px or -10px',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'header_area_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'logo_margin_bottom',
        'label'       => 'Logo Margin Bottom',
        'desc'        => 'Logo margin from bottom. Remember to add px value after the number. Example: 10px or -10px',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'header_area_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'valise_bg_type',
        'label'       => 'Background Type',
        'desc'        => 'Select the background type you want, either full image or pattern.',
        'std'         => '',
        'type'        => 'select',
        'section'     => 'background_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
		'choices'     => array( 
          array(
            'value'       => 'image',
            'label'       => 'Image',
            'src'         => ''
          ),
          array(
            'value'       => 'pattern',
            'label'       => 'Pattern',
            'src'         => ''
          )
        ),
      ),
	  array(
        'id'          => 'valise_default_bgimage',
        'label'       => 'Default Background',
        'desc'        => 'Upload your default background image',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'background_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_archive_bgimage',
        'label'       => 'Archive Background',
        'desc'        => 'Upload your archive background image',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'background_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_search_bgimage',
        'label'       => 'Search Background',
        'desc'        => 'Upload your search background image',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'background_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_notfound_bgimage',
        'label'       => '404 Not Found Background',
        'desc'        => 'Upload your 404 not found background image',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'background_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'title_background',
        'label'       => 'Title area background',
        'desc'        => 'This option is for title area background. You can either use color or upload a background image. Leave blank for default setting.',
        'std'         => '',
        'type'        => 'background',
        'section'     => 'background_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'title_area_background_size',
        'label'       => 'Title area background type',
        'desc'        => 'Scale image option keeps fixed image width at 100%. Use it for single non-repeated images with fixed position. (Image aspect ratio should be 3:2 or wider &amp; image should be at least 250px in height.)',
        'std'         => '',
        'type'        => 'select',
        'section'     => 'background_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'auto',
            'label'       => 'Default',
            'src'         => ''
          ),
          array(
            'value'       => 'contain',
            'label'       => 'Scale image',
            'src'         => ''
          )
        ),
      ),
      
	  

	array(
        'id'          => 'valise_primary_color',
        'label'       => 'Primary Color',
        'desc'        => 'Click input field for colorpicker or enter your custom value',
        'std'         => '#34b78b',
        'type'        => 'colorpicker',
        'section'     => 'color_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'valise_secondary_color',
        'label'       => 'Secondary Color',
        'desc'        => 'Click input field for colorpicker or enter your custom value',
        'std'         => '#273039',
        'type'        => 'colorpicker',
        'section'     => 'color_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'valise_menu_text_color',
        'label'       => 'Menu Text',
        'desc'        => 'Click input field for colorpicker or enter your custom value',
        'std'         => '#25292b',
        'type'        => 'colorpicker',
        'section'     => 'color_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'valise_header_text_color',
        'label'       => 'Heading Text color (Content Area)',
        'desc'        => 'Enter your header text color',
        'std'         => '#1a1a1a',
        'type'        => 'colorpicker',
        'section'     => 'color_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'valise_sidebar_text_color',
        'label'       => 'Sidebar Heading Color',
        'desc'        => 'Click input field for colorpicker or enter your custom value',
        'std'         => '#848b8f',
        'type'        => 'colorpicker',
        'section'     => 'color_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'valise_footer_header_color',
        'label'       => 'Footer heading color',
        'desc'        => 'Click input field for colorpicker or enter your custom value',
        'std'         => '#34b78b',
        'type'        => 'colorpicker',
        'section'     => 'color_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),



      array(
        'id'          => 'google_menu_font',
        'label'       => 'Menu font',
        'desc'        => 'Font used in theme preview: Source Sans Pro',
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'typography_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => 'hide-color-field'
      ),
      array(
        'id'          => 'font_preview_1',
        'label'       => 'Menu font preview',
        'desc'        => 'Only font-family preview! (font-weight, letter-spacing, text-transform doesn\'t affect this preview)',
        'std'         => 'Grumpy wizards make toxic brew for the evil Queen and Jack.',
        'type'        => 'textarea-simple',
        'section'     => 'typography_heading',
        'rows'        => '1',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => 'menu_font_preview'
      ),
      array(
        'id'          => 'google_page_font',
        'label'       => 'Page title font',
        'desc'        => 'Font used in theme preview: Open Sans',
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'typography_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'page_title_font_preview',
        'label'       => 'Page title font preview',
        'desc'        => 'Only font-family preview! (font-weight, letter-spacing, text-transform doesn\'t affect this preview)',
        'std'         => 'Grumpy wizards make toxic brew for the evil Queen and Jack.',
        'type'        => 'textarea-simple',
        'section'     => 'typography_heading',
        'rows'        => '1',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => 'title_preview'
      ),
      array(
        'id'          => 'google_fonts_headings',
        'label'       => 'Content heading font',
        'desc'        => 'Font used in theme preview: Open Sans',
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'typography_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'content_heading_font_preview',
        'label'       => 'Content heading font preview',
        'desc'        => 'Only font-family preview! (font-weight, letter-spacing, text-transform doesn\'t affect this preview)',
        'std'         => 'Grumpy wizards make toxic brew for the evil Queen and Jack.',
        'type'        => 'textarea-simple',
        'section'     => 'typography_heading',
        'rows'        => '1',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => 'heading_preview'
      ),


      array(
        'id'          => 'google_body_font',
        'label'       => 'Content font',
        'desc'        => 'Font used in theme preview: PT Sans',
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'typography_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'body_font_preview',
        'label'       => 'Content font preview',
        'desc'        => 'Only font-family preview! (font-weight, letter-spacing, text-transform doesn\'t affect this preview)',
        'std'         => 'Grumpy wizards make toxic brew for the evil Queen and Jack.',
        'type'        => 'textarea-simple',
        'section'     => 'typography_heading',
        'rows'        => '1',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => 'content_preview'
      ),
      array(
        'id'          => 'google_custom_font',
        'label'       => 'Custom font for element',
        'desc'        => 'Font used in theme preview: Montserrat <br />
To use custom font add CSS class - "custom-font" ( class="custom-font" ) to an element.',
        'std'         => '',
        'type'        => 'typography',
        'section'     => 'typography_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'custom_font_preview',
        'label'       => 'Custom font for element preview',
        'desc'        => 'Only font-family preview! (font-weight, letter-spacing, text-transform doesn\'t affect this preview)',
        'std'         => 'Grumpy wizards make toxic brew for the evil Queen and Jack.',
        'type'        => 'textarea-simple',
        'section'     => 'typography_heading',
        'rows'        => '1',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => 'custom_font_preview'
      ),
      array(
        'id'          => 'font_sizes',
        'label'       => 'Font sizes',
        'desc'        => '<h1>Change font sizes</h1>',
        'std'         => '',
        'type'        => 'textblock',
        'section'     => 'typography_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'menu_font_size',
        'label'       => 'Menu font size',
        'desc'        => 'Remember to add "px" at the end. Example: 15px 
<br /> Leave blank for default value.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'typography_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'content_text_font_size',
        'label'       => 'Body font size',
        'desc'        => 'Remember to add "px" at the end. Example: 15px 
<br /> Leave blank for default value.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'typography_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'h1_size',
        'label'       => 'H1 font size',
        'desc'        => 'Remember to add "px" at the end. Example: 15px 
<br /> Leave blank for default value. Defaut value: 30px',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'typography_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'h2_size',
        'label'       => 'H2 font size',
        'desc'        => 'Remember to add "px" at the end. Example: 15px 
<br /> Leave blank for default value. Defaut value: 24px',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'typography_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'h3_size',
        'label'       => 'H3 font size',
        'desc'        => 'Remember to add "px" at the end. Example: 15px 
<br /> Leave blank for default value. Defaut value: 20px',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'typography_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'h4_size',
        'label'       => 'H4 font size',
        'desc'        => 'Remember to add "px" at the end. Example: 15px 
<br /> Leave blank for default value. Defaut value: 18px',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'typography_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'h5_size',
        'label'       => 'H5 font size',
        'desc'        => 'Remember to add "px" at the end. Example: 15px 
<br /> Leave blank for default value. Defaut value: 16px',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'typography_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'h6_size',
        'label'       => 'H6 font size',
        'desc'        => 'Remember to add "px" at the end. Example: 15px 
<br /> Leave blank for default value. Defaut value: 14px',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'typography_heading',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'valise_footer_widget_hide',
        'label'       => 'Hide Footer Widgets',
        'desc'        => 'Hide footer widgets at the bottom.',
        'std'         => '',
        'type'        => 'checkbox',
        'section'     => 'footer_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'disable_footer_widgets',
            'label'       => 'Disable Footer Widgets',
            'src'         => ''
          )
        ),
      ),
      array(
        'id'          => 'copyright_area_text',
        'label'       => 'Copyright area text',
        'desc'        => 'This text will appear on the bottom left of your website.',
        'std'         => '&copy; 2013 All Rights Reserved. Theme by <a href="http://defatch.com" target="_blank"> Defatch.</a>',
        'type'        => 'textarea-simple',
        'section'     => 'footer_options',
        'rows'        => '6',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  
	  array(
        'id'          => 'valise_facebook',
        'label'       => 'Facebook Link',
        'desc'        => 'Paste your facebook link',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_media',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_twitter',
        'label'       => 'Twitter Link',
        'desc'        => 'Paste your twitter link',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_media',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_dribbble',
        'label'       => 'Dribbble Link',
        'desc'        => 'Paste your dribbble link',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_media',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_linkedin',
        'label'       => 'LinkedIn Link',
        'desc'        => 'Paste your LinkedIn link',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_media',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_google',
        'label'       => 'Google Plus Link',
        'desc'        => 'Paste your Google Plus link',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_media',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_instagram',
        'label'       => 'Instagram Link',
        'desc'        => 'Paste your Instagram link',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_media',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_skype',
        'label'       => 'Skype Link',
        'desc'        => 'Paste your Skype link',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_media',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_youtube',
        'label'       => 'Youtube Link',
        'desc'        => 'Paste your Youtube link',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_media',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'portfolio_title',
        'label'       => 'Portfolio',
        'desc'        => '<h1>Portfolio</h1>',
        'std'         => '',
        'type'        => 'textblock',
        'section'     => 'cpt',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'valise_portfolio_cpt',
        'label'       => 'Portfolio',
        'desc'        => 'Change the portfolio name custom post type.',
        'std'         => 'Portfolio',
        'type'        => 'text',
        'section'     => 'cpt',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_portfolio_cpt_slug',
        'label'       => 'Portfolio Slug',
        'desc'        => 'Change the portfolio slug custom post type.',
        'std'         => 'portfolio-item',
        'type'        => 'text',
        'section'     => 'cpt',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_portfolio_add',
        'label'       => 'Portfolio Add',
        'desc'        => 'Change the portfolio add item label custom post type.',
        'std'         => 'Add New Portfolio',
        'type'        => 'text',
        'section'     => 'cpt',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_portfolio_edit',
        'label'       => 'Portfolio Edit Item',
        'desc'        => 'Change the portfolio edit item label custom post type.',
        'std'         => 'Edit Portfolio',
        'type'        => 'text',
        'section'     => 'cpt',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_portfolio_tax_slug',
        'label'       => 'Portfolio Taxonomy Slug',
        'desc'        => 'Change the portfolio taxonomy slug rewrite.',
        'std'         => 'portfolio-category',
        'type'        => 'text',
        'section'     => 'cpt',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'services_title',
        'label'       => 'Services',
        'desc'        => '<h1>Services</h1>',
        'std'         => '',
        'type'        => 'textblock',
        'section'     => 'cpt',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'valise_services_cpt',
        'label'       => 'Services',
        'desc'        => 'Change the services name custom post type.',
        'std'         => 'Services',
        'type'        => 'text',
        'section'     => 'cpt',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_services_cpt_slug',
        'label'       => 'Services Slug',
        'desc'        => 'Change the services slug custom post type.',
        'std'         => 'services-item',
        'type'        => 'text',
        'section'     => 'cpt',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_services_add',
        'label'       => 'Services Add Item',
        'desc'        => 'Change the services add item label custom post type.',
        'std'         => 'Add New Services',
        'type'        => 'text',
        'section'     => 'cpt',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_services_edit',
        'label'       => 'Services Edit Item',
        'desc'        => 'Change the services edit item label custom post type.',
        'std'         => 'Edit Services',
        'type'        => 'text',
        'section'     => 'cpt',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  
	  array(
        'id'          => 'testimonials_title',
        'label'       => 'Testimonials',
        'desc'        => '<h1>Testimonials</h1>',
        'std'         => '',
        'type'        => 'textblock',
        'section'     => 'cpt',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'valise_testimonial_cpt',
        'label'       => 'Testimonials',
        'desc'        => 'Change the testimonial name custom post type.',
        'std'         => 'Testimonials',
        'type'        => 'text',
        'section'     => 'cpt',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_testimonial_cpt_slug',
        'label'       => 'Testimonial Slug',
        'desc'        => 'Change the testimonial slug custom post type.',
        'std'         => 'testimonial-item',
        'type'        => 'text',
        'section'     => 'cpt',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_testimonial_add',
        'label'       => 'Testimonial Add Item',
        'desc'        => 'Change the testimonial add item label custom post type.',
        'std'         => 'Add New Testimonial',
        'type'        => 'text',
        'section'     => 'cpt',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_testimonial_edit',
        'label'       => 'Testimonial Edit Item',
        'desc'        => 'Change the testimonial edit item label custom post type.',
        'std'         => 'Edit Testimony',
        'type'        => 'text',
        'section'     => 'cpt',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  
	  
	  
      array(
        'id'          => 'valise_search_header',
        'label'       => 'Search Header',
        'desc'        => 'Enter your header label for search page.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'translation',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_search_input',
        'label'       => 'Search Input Label',
        'desc'        => 'Enter your search input text label.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'translation',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_notfound_header',
        'label'       => '404 Header',
        'desc'        => 'Enter your header label for 404 top label.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'translation',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_notfound_heading',
        'label'       => '404 Heading',
        'desc'        => 'Enter your 404 page heading.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'translation',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_notfound_text',
        'label'       => '404 Text',
        'desc'        => 'Enter your 404 page content text.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'translation',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'valise_notfound_button',
        'label'       => '404 Button',
        'desc'        => 'Enter your text for 404 button label.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'translation',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'valise_custom_css',
        'label'       => 'Custom CSS',
        'desc'        => 'Add any of your custom CSS here.',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'css',
        'rows'        => '20',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  
	  
	  array(
        'id'          => 'welcome_text_title',
        'label'       => 'Welcome Text',
        'desc'        => '<h1>Welcome Text</h1>',
        'std'         => '',
        'type'        => 'textblock',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_welcome_hide',
        'label'       => 'Hide',
        'desc'        => 'check the box if you want to hide the welcome text',
        'std'         => '',
        'type'        => 'checkbox',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'disable_welcome_text',
            'label'       => 'Disable Welcome Text',
            'src'         => ''
          )
        )
    ),
	array(
        'id'          => 'valise_welcome_header',
        'label'       => 'Header',
        'desc'        => 'Enter your welcome header text',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'home_page',
        'rows'        => '6',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	 
	array(
        'id'          => 'services_title',
        'label'       => 'Services',
        'desc'        => '<h1>Services</h1>',
        'std'         => '',
        'type'        => 'textblock',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	array(
        'id'          => 'valise_services_hide',
        'label'       => 'Hide',
        'desc'        => 'check the box if you want to hide the services list.',
        'std'         => '',
        'type'        => 'checkbox',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'disable_services',
            'label'       => 'Disable Services Section',
            'src'         => ''
          )
        )
    ),
	array(
        'id'          => 'valise_num_hservices',
        'label'       => 'Limit',
        'desc'        => 'Enter number of services you want to display (min:3)',
        'std'         => '3',
        'type'        => 'text',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_services_order',
        'label'       => 'Order Type',
        'desc'        => 'Select your services order type (ASC or DESC)',
        'std'         => '',
        'type'        => 'select',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
		'choices'     => array( 
          array(
            'value'       => 'ASC',
            'label'       => 'ASC',
            'src'         => ''
          ),
          array(
            'value'       => 'DESC',
            'label'       => 'DESC',
            'src'         => ''
          )
        ),
      ),
	array(
        'id'          => 'valise_services_orderby',
        'label'       => 'OrderBy Type',
        'desc'        => 'Select your orderby parameter for services',
        'std'         => '',
        'type'        => 'select',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
		'choices'     => array( 
          array(
            'value'       => 'ID',
            'label'       => 'ID',
            'src'         => ''
          ),
          array(
            'value'       => 'author',
            'label'       => 'Author',
            'src'         => ''
          ),
		  array(
            'value'       => 'title',
            'label'       => 'Title',
            'src'         => ''
          ),
          array(
            'value'       => 'name',
            'label'       => 'Name',
            'src'         => ''
          ),
		  array(
            'value'       => 'date',
            'label'       => 'Date',
            'src'         => ''
          ),
          array(
            'value'       => 'modified',
            'label'       => 'Modified',
            'src'         => ''
          ),
		  array(
            'value'       => 'parent',
            'label'       => 'Parent',
            'src'         => ''
          ),
          array(
            'value'       => 'rand',
            'label'       => 'Rand',
            'src'         => ''
          ),
		  array(
            'value'       => 'comment_count',
            'label'       => 'Comment Count',
            'src'         => ''
          ),
          array(
            'value'       => 'menu_order',
            'label'       => 'Menu Order',
            'src'         => ''
          )
        ),
      ),
	
	array(
        'id'          => 'testimonials_title',
        'label'       => 'Testimonials Section',
        'desc'        => '<h1>Testimonials Section</h1>',
        'std'         => '',
        'type'        => 'textblock',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_testimonial_hide',
        'label'       => 'Hide',
        'desc'        => 'check the box if you want to hide the testimonials section',
        'std'         => '',
        'type'        => 'checkbox',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'disable_testimonial',
            'label'       => 'Disable Testimonials Section',
            'src'         => ''
          )
        )
    ),
	array(
        'id'          => 'valise_testimonial_bg',
        'label'       => 'Background Image',
        'desc'        => 'Upload testimonials background image',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_num_testimonial',
        'label'       => 'Limit',
        'desc'        => 'Enter number of services you want to display (min:3)',
        'std'         => '2',
        'type'        => 'text',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_testimonial_order',
        'label'       => 'Order Type',
        'desc'        => 'Select your testimonial order type (ASC or DESC)',
        'std'         => '',
        'type'        => 'select',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
		'choices'     => array( 
          array(
            'value'       => 'ASC',
            'label'       => 'ASC',
            'src'         => ''
          ),
          array(
            'value'       => 'DESC',
            'label'       => 'DESC',
            'src'         => ''
          )
        ),
      ),
	array(
        'id'          => 'valise_testimonial_orderby',
        'label'       => 'OrderBy Type',
        'desc'        => 'Select your orderby parameter for testimonial',
        'std'         => '',
        'type'        => 'select',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
		'choices'     => array( 
          array(
            'value'       => 'ID',
            'label'       => 'ID',
            'src'         => ''
          ),
          array(
            'value'       => 'author',
            'label'       => 'Author',
            'src'         => ''
          ),
		  array(
            'value'       => 'title',
            'label'       => 'Title',
            'src'         => ''
          ),
          array(
            'value'       => 'name',
            'label'       => 'Name',
            'src'         => ''
          ),
		  array(
            'value'       => 'date',
            'label'       => 'Date',
            'src'         => ''
          ),
          array(
            'value'       => 'modified',
            'label'       => 'Modified',
            'src'         => ''
          ),
		  array(
            'value'       => 'parent',
            'label'       => 'Parent',
            'src'         => ''
          ),
          array(
            'value'       => 'rand',
            'label'       => 'Rand',
            'src'         => ''
          ),
		  array(
            'value'       => 'comment_count',
            'label'       => 'Comment Count',
            'src'         => ''
          ),
          array(
            'value'       => 'menu_order',
            'label'       => 'Menu Order',
            'src'         => ''
          )
        ),
      ),
	
	
	array(
        'id'          => 'blog_title',
        'label'       => 'Blog Section',
        'desc'        => '<h1>Blog Section</h1>',
        'std'         => '',
        'type'        => 'textblock',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_blog_hide',
        'label'       => 'Hide',
        'desc'        => 'check the box if you want to hide the blog section from homepage',
        'std'         => '',
        'type'        => 'checkbox',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'disable_blog_sec',
            'label'       => 'Disable Blog Section',
            'src'         => ''
          )
        )
    ),
	array(
        'id'          => 'valise_blog_hheader',
        'label'       => 'Blog Title',
        'desc'        => 'Enter your blog title',
        'std'         => 'Latest Articles',
        'type'        => 'text',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_num_hblog',
        'label'       => 'Limit',
        'desc'        => 'Enter number of news you want to display (min:3)',
        'std'         => '3',
        'type'        => 'text',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_blog_text',
        'label'       => 'Button Text',
        'desc'        => 'Enter blog view all button text',
        'std'         => 'View All',
        'type'        => 'text',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_blog_link',
        'label'       => 'Button Link',
        'desc'        => 'Enter blog view all button link',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	
	 array(
        'id'          => 'portfolio_title',
        'label'       => 'Featured Portfolio',
        'desc'        => '<h1>Featured Portfolio</h1>',
        'std'         => '',
        'type'        => 'textblock',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),	  
	  array(
        'id'          => 'valise_portfolio_hide',
        'label'       => 'Hide',
        'desc'        => 'check the box if you want to hide the portfolio list.',
        'std'         => '',
        'type'        => 'checkbox',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'disable_featured_port',
            'label'       => 'Disable Featured Portfolio',
            'src'         => ''
          )
        )
	),
	array(
        'id'          => 'valise_hportfolio_header',
        'label'       => 'Featured Works Title',
        'desc'        => 'Enter your portfolio header for homepage list.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_portfolio_text',
        'label'       => 'View All Button Text',
        'desc'        => 'Enter your view all button text.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_portfolio_link',
        'label'       => 'View All Button Link',
        'desc'        => 'Enter your view all button link.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_num_hportfolio',
        'label'       => 'Limit',
        'desc'        => 'Enter number of portfolio you want to display (min:5)',
        'std'         => '6',
        'type'        => 'text',
        'section'     => 'home_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	
	
	array(
        'id'          => 'blog_page_title',
        'label'       => 'Blog Page',
        'desc'        => '<h1>Blog Page</h1>',
        'std'         => '',
        'type'        => 'textblock',
        'section'     => 'blog_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_blog_sidebar',
        'label'       => 'Sidebar Position',
        'desc'        => 'Select your sidebar position',
        'std'         => '',
        'type'        => 'select',
        'section'     => 'blog_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
		'choices'     => array( 
          array(
            'value'       => 'Right',
            'label'       => 'Right',
            'src'         => ''
          ),
          array(
            'value'       => 'Left',
            'label'       => 'Left',
            'src'         => ''
          )
        ),
      ),
	array(
        'id'          => 'valise_num_blog',
        'label'       => 'Limit',
        'desc'        => 'Enter blog items you want to display in blog page',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'blog_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_read_more_text',
        'label'       => 'Read More Button Text',
        'desc'        => 'Enter blog read more button text',
        'std'         => 'Read More',
        'type'        => 'text',
        'section'     => 'blog_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_featured_img',
        'label'       => 'Set Default Featured Image',
        'desc'        => 'if your blog posts not have featured image it will apply there',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'blog_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_hide_excerpt',
        'label'       => 'Hide Excerpt',
        'desc'        => 'Hide your excerpt or brief description of your post',
        'std'         => '',
        'type'        => 'checkbox',
        'section'     => 'blog_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'disable_excerpt_text',
            'label'       => 'Disable Excerpt Text',
            'src'         => ''
          )
        )
    ),
	array(
        'id'          => 'valise_blog_pnav',
        'label'       => 'Blog Navigation',
        'desc'        => 'Select your blog page navigation style',
        'std'         => '',
        'type'        => 'select',
        'section'     => 'blog_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
		'choices'     => array( 
          array(
            'value'       => 'Pagination',
            'label'       => 'Pagination',
            'src'         => ''
          ),
          array(
            'value'       => 'Next Previous Link',
            'label'       => 'Next Previous Link',
            'src'         => ''
          )
        ),
    ),
	array(
        'id'          => 'single_post_title',
        'label'       => 'Single Post',
        'desc'        => '<h1>Single Post</h1>',
        'std'         => '',
        'type'        => 'textblock',
        'section'     => 'blog_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_hide_author',
        'label'       => 'Hide Author',
        'desc'        => 'Hide author box in blog single page',
        'std'         => '',
        'type'        => 'checkbox',
        'section'     => 'blog_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'disable_author_box',
            'label'       => 'Disable Author Box',
            'src'         => ''
          )
        )
    ),
	array(
        'id'          => 'valise_comment_cheader',
        'label'       => 'Comment Count Header',
        'desc'        => 'Enter your header label for comment count box',
        'std'         => 'Latest comments',
        'type'        => 'text',
        'section'     => 'blog_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_comment_notify',
        'label'       => 'Comment Count Notification',
        'desc'        => 'Enter your header label for comment count notification',
        'std'         => 'No Comments',
        'type'        => 'text',
        'section'     => 'blog_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_comment_fheader',
        'label'       => 'Comment Form Header',
        'desc'        => 'Enter your header label for comment form box',
        'std'         => 'Leave a comment',
        'type'        => 'text',
        'section'     => 'blog_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'portfolio_page_title',
        'label'       => 'Portfolio Page',
        'desc'        => '<h1>Portfolio Page</h1>',
        'std'         => '',
        'type'        => 'textblock',
        'section'     => 'portfolio_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_num_portfolio',
        'label'       => 'Limit',
        'desc'        => 'Enter portfolio items you want to display in portfolio page',
        'std'         => '6',
        'type'        => 'text',
        'section'     => 'portfolio_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_album_order',
        'label'       => 'Order Type',
        'desc'        => 'Select your portfolio order type (ASC or DESC)',
        'std'         => '',
        'type'        => 'select',
        'section'     => 'portfolio_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
		'choices'     => array( 
          array(
            'value'       => 'ASC',
            'label'       => 'ASC',
            'src'         => ''
          ),
          array(
            'value'       => 'DESC',
            'label'       => 'DESC',
            'src'         => ''
          )
        ),
      ),
	array(
        'id'          => 'valise_album_orderby',
        'label'       => 'OrderBy Type',
        'desc'        => 'Select your orderby parameter for portfolio',
        'std'         => '',
        'type'        => 'select',
        'section'     => 'portfolio_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
		'choices'     => array( 
          array(
            'value'       => 'ID',
            'label'       => 'ID',
            'src'         => ''
          ),
          array(
            'value'       => 'author',
            'label'       => 'Author',
            'src'         => ''
          ),
		  array(
            'value'       => 'title',
            'label'       => 'Title',
            'src'         => ''
          ),
          array(
            'value'       => 'name',
            'label'       => 'Name',
            'src'         => ''
          ),
		  array(
            'value'       => 'date',
            'label'       => 'Date',
            'src'         => ''
          ),
          array(
            'value'       => 'modified',
            'label'       => 'Modified',
            'src'         => ''
          ),
		  array(
            'value'       => 'parent',
            'label'       => 'Parent',
            'src'         => ''
          ),
          array(
            'value'       => 'rand',
            'label'       => 'Rand',
            'src'         => ''
          ),
		  array(
            'value'       => 'comment_count',
            'label'       => 'Comment Count',
            'src'         => ''
          ),
          array(
            'value'       => 'menu_order',
            'label'       => 'Menu Order',
            'src'         => ''
          )
        ),
      ),
	array(
        'id'          => 'valise_pcategory_label',
        'label'       => 'Category All Text',
        'desc'        => 'Enter your portfolio category view all label',
        'std'         => 'All',
        'type'        => 'text',
        'section'     => 'portfolio_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_all_link',
        'label'       => 'Category All Link',
        'desc'        => 'Enter your portfolio category view all link',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'portfolio_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_portfolio_pnav',
        'label'       => 'Portfolio Navigation',
        'desc'        => 'Select your portfolio page navigation style',
        'std'         => '',
        'type'        => 'select',
        'section'     => 'portfolio_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
		'choices'     => array( 
          array(
            'value'       => 'Pagination',
            'label'       => 'Pagination',
            'src'         => ''
          ),
          array(
            'value'       => 'Next Previous Link',
            'label'       => 'Next Previous Link',
            'src'         => ''
          )
        ),
    ),
	array(
        'id'          => 'single_page_title',
        'label'       => 'Single Page',
        'desc'        => '<h1>Single Page</h1>',
        'std'         => '',
        'type'        => 'textblock',
        'section'     => 'portfolio_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
    ),
	array(
        'id'          => 'valise_portfolio_thumb_slider',
        'label'       => 'Disable Portfolio Image',
        'desc'        => 'Check the box to exclude images on single portfolio',
        'std'         => '',
        'type'        => 'checkbox',
        'section'     => 'portfolio_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'disable_portfolio_image',
            'label'       => 'Disable Portfolio Image',
            'src'         => ''
          )
        )
    ),
	array(
        'id'          => 'port_slide_icons',
        'label'       => 'Disable Portfolio Slide Icons',
        'desc'        => 'Check the box to hide slide icons',
        'std'         => '',
        'type'        => 'checkbox',
        'section'     => 'portfolio_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'choices'     => array( 
          array(
            'value'       => 'disable_slide_icons',
            'label'       => 'Disable Slide Icons',
            'src'         => ''
          )
        )
    ),
	array(
        'id'          => 'valise_attachment_order',
        'label'       => 'Order Type',
        'desc'        => 'Select your ordering style in portfolio page for album images',
        'std'         => '',
        'type'        => 'select',
        'section'     => 'portfolio_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
		'choices'     => array( 
          array(
            'value'       => 'ASC',
            'label'       => 'ASC',
            'src'         => ''
          ),
          array(
            'value'       => 'DESC',
            'label'       => 'DESC',
            'src'         => ''
          )
        ),
      ),
	array(
        'id'          => 'valise_attachment_orderby',
        'label'       => 'OrderBy Type',
        'desc'        => 'Select your orderby parameter in portfolio page for album images',
        'std'         => '',
        'type'        => 'select',
        'section'     => 'portfolio_page',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
		'choices'     => array( 
          array(
            'value'       => 'ID',
            'label'       => 'ID',
            'src'         => ''
          ),
          array(
            'value'       => 'author',
            'label'       => 'Author',
            'src'         => ''
          ),
		  array(
            'value'       => 'title',
            'label'       => 'Title',
            'src'         => ''
          ),
          array(
            'value'       => 'name',
            'label'       => 'Name',
            'src'         => ''
          ),
		  array(
            'value'       => 'date',
            'label'       => 'Date',
            'src'         => ''
          ),
          array(
            'value'       => 'modified',
            'label'       => 'Modified',
            'src'         => ''
          ),
		  array(
            'value'       => 'parent',
            'label'       => 'Parent',
            'src'         => ''
          ),
          array(
            'value'       => 'rand',
            'label'       => 'Rand',
            'src'         => ''
          ),
		  array(
            'value'       => 'comment_count',
            'label'       => 'Comment Count',
            'src'         => ''
          ),
          array(
            'value'       => 'menu_order',
            'label'       => 'Menu Order',
            'src'         => ''
          )
        ),
      )
      
    )
  );
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( 'option_tree_settings_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( 'option_tree_settings', $custom_settings ); 
  }
  
}