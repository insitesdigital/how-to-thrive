<?php
function filter_radio_images( $array, $field_id ) {
  
  if ( $field_id == 'theme_layout' ) {
    $array = array(
	  array(
        'value'   => 'container-boxed',
        'label'   => __( 'Boxed', 'option-tree' ),
        'src'     => OT_URL . '/assets/images/layout/boxed-attached.png'
      ),
	  array(
        'value'   => 'container',
        'label'   => __( 'Full width', 'option-tree' ),
        'src'     => OT_URL . '/assets/images/layout/full-width.png'
      )
    );
  }  



  return $array;
  
}
add_filter( 'ot_radio_images', 'filter_radio_images', 10, 2 );
?>