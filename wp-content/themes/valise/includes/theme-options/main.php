<?php
	function valise_add_init() {
		if(is_admin()) {
			$file_dir = get_template_directory_uri();			

			wp_register_style('font-awesome', $file_dir."/includes/font-awesome/css/font-awesome.min.css");
			wp_enqueue_style("admin", $file_dir."/includes/theme-options/assets/css/style.css", false, "1.0", "all");
			wp_enqueue_style('font-awesome');
		}			
	}
	add_action('admin_init', 'valise_add_init');
?>