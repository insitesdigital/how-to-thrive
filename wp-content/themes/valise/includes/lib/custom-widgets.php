<?php
	include get_template_directory() . '/includes/widgets/widget-follow-us.php';
	include get_template_directory() . '/includes/widgets/widget-contact.php';
	include get_template_directory() . '/includes/widgets/widget-video.php';
	include get_template_directory() . '/includes/widgets/widget-flickr.php';
	include get_template_directory() . '/includes/widgets/widget-popular.php';
	include get_template_directory() . '/includes/widgets/widget-dribbble.php';


	/**********************************************
	SIDEBAR WIDGETS
	***********************************************/
	
	function valise_sidebar_widgets_init() {
	
		register_sidebar( array(
			'name' => __( 'Top Left Sidebar', CSB_THEME_NAME ),
			'id' => 'top_left_sidebar',
			'description' => __( 'Top left sidebar of the theme', CSB_THEME_NAME ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<h5>',
			'after_title' => '</h5>',
		) );
		
		register_sidebar( array(
			'name' => __( 'Top Right Sidebar', CSB_THEME_NAME ),
			'id' => 'top_right_sidebar',
			'description' => __( 'Top right sidebar of the theme', CSB_THEME_NAME ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<h5>',
			'after_title' => '</h5>',
		) );
		
		register_sidebar( array(
			'name' => __( 'Page Sidebar', CSB_THEME_NAME ),
			'id' => 'sidebar',
			'description' => __( 'Main sidebar of the theme', CSB_THEME_NAME ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<h5>',
			'after_title' => '</h5>',
		) );

		register_sidebar( array(
			'name' => __( 'Blog Sidebar', CSB_THEME_NAME ),
			'id' => 'blog_sidebar',
			'description' => __( 'Blog widgets of the theme', CSB_THEME_NAME ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<h5>',
			'after_title' => '</h5>',
		) );

		register_sidebar( array(
			'name' => __( 'Contact Sidebar', CSB_THEME_NAME ),
			'id' => 'contact_sidebar',
			'description' => __( 'Contact widgets of the theme', CSB_THEME_NAME ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<h5>',
			'after_title' => '</h5>',
		) );

		register_sidebar( array(
			'name' => __( 'Footer One', CSB_THEME_NAME ),
			'id' => 'footer_one',
			'description' => __( 'Footer column widget of the theme', CSB_THEME_NAME ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<h5>',
			'after_title' => '</h5>',
		) );

		register_sidebar( array(
			'name' => __( 'Footer Two', CSB_THEME_NAME ),
			'id' => 'footer_two',
			'description' => __( 'Footer column widget of the theme', CSB_THEME_NAME ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<h5>',
			'after_title' => '</h5>',
		) );

		register_sidebar( array(
			'name' => __( 'Footer Three', CSB_THEME_NAME ),
			'id' => 'footer_three',
			'description' => __( 'Footer column widget of the theme', CSB_THEME_NAME ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<h5>',
			'after_title' => '</h5>',
		) );

		register_sidebar( array(
			'name' => __( 'Footer Four', CSB_THEME_NAME ),
			'id' => 'footer_four',
			'description' => __( 'Footer column widget of the theme', CSB_THEME_NAME ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<h5>',
			'after_title' => '</h5>',
		) );
				
	}
	add_action( 'widgets_init', 'valise_sidebar_widgets_init' );
?>