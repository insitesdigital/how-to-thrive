<?php
	/***************
	PROJECTS
	***************/

	function valise_portfolio_list() {
		global $post, $portfolio_page_url;
		if(ot_get_option('valise_portfolio_hide')== "") {
			?>
				<!--PROJECTS-->
				<section class="home-projects">
					<div class="inside project-block clear">
						<h2><?php if(ot_get_option('valise_hportfolio_header')!="") { echo ot_get_option('valise_hportfolio_header'); } else { _e( 'Featured Work', CSB_THEME_NAME ); } ?></h2>
						<a href="<?php if(ot_get_option('valise_portfolio_link')!="") { echo ot_get_option('valise_portfolio_link'); } else { echo $portfolio_page_url; } ?>" class="view-all">
							<?php if(ot_get_option('valise_portfolio_text')!="") { echo ot_get_option('valise_portfolio_text'); } else { _e( 'View All', CSB_THEME_NAME ); } ?>
						</a>

						<div class="mansory-featured">
							<ul class="clear"> 
								<?php
									$album_order = get_option('valise_album_order');
									$album_orderby = get_option('valise_album_orderby');
									$album_limit = ot_get_option('valise_num_hportfolio');

									$args = array( 'post_type' => 'portfolio', 'orderby' => $album_orderby, 'order' => $album_order, 'posts_per_page' => $album_limit, 'meta_query' => array( array( 'key' => 'exclude', 'value' => 'on', 'compare' => '!=' ) ) );		
									$wp_query = new WP_Query( $args );	
									while ($wp_query->have_posts()) : $wp_query->the_post(); 
									
									$thumb_size = get_post_meta($post->ID, 'thumb_size', true);
										?>
											<li id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
												<div class="port-hover <?php echo $thumb_size ?>">
													<?php 
                                                        if ( post_password_required() ) {
                                                            ?>
                                                            <div class="password-protect-thumb port-2cols"><h3><?php _e( 'Password Protected', CSB_THEME_NAME ); ?></h3></div>
                                                            <?php
                                                        }
                                                        $thumb_id = get_post_thumbnail_id();
                                                        $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
                                                    ?>
                                                    
                                                    <?php if (get_post_meta($post->ID,'thumb_size', true) == 'thumb_large') {
                                                                    if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                                                                      the_post_thumbnail('cs-size-large');
                                                                    }
                                                            }
                                                        ?>
                                                        
                                                        <?php if (get_post_meta($post->ID,'thumb_size', true) == 'thumb_medium') {
                                                                    if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                                                                      the_post_thumbnail('cs-size-medium');
                                                                    }
                                                            }
                                                        ?>
                                                        
                                                        <?php if (get_post_meta($post->ID,'thumb_size', true) == 'thumb_verticle') {
                                                                    if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                                                                      the_post_thumbnail('cs-size-verticle');
                                                                    }
                                                            }
                                                        ?>
                                                        
                                                        <?php if (get_post_meta($post->ID,'thumb_size', true) == 'thumb_horizontal') {
                                                                    if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                                                                      the_post_thumbnail('cs-size-horizontal');
                                                                    }
                                                            }
                                                        ?>
                                                    
                                                    
                                                    <div class="overlay">
                                                        <a href="<?php the_permalink(); ?>" class="details"><i class="icon-share-alt"></i>Details</a>
                                                        <a href="<?php echo $thumb_url[0]; ?>" class="zoom fancybox"><i class="icon-eye-open"></i>View</a>
                                                    </div>
                                                </div>									
												<h5>
													<?php the_title(); ?>

												</h5>						
											</li>
										<?php
									endwhile;	
								?>
							</ul>	
						</div>	
					</div>
				</section>
			<?php
		}		
	}


	/******************
	PROJECTS + SLIDER
	******************/

	function valise_portfolio_slider() {
		global $post;
		if(get_option('valise_portfolio_hide')!="true") {
			?>
				<!--PROJECTS-->
				<section class="home-projects-slide">
					<div class="project-slide-block">
						<div class="home-project-flexslider">
							<ul class="slides"> 
								<?php
									$album_order = get_option('valise_album_order');
									$album_orderby = get_option('valise_album_orderby');
									$album_limit = ot_get_option('valise_num_hportfolio');

									$args = array( 'post_type' => 'portfolio', 'orderby' => $album_orderby, 'order' => $album_order, 'posts_per_page' => $album_limit, 'meta_query' => array( array( 'key' => 'exclude', 'value' => 'on', 'compare' => '!=' ) ) );		
									$wp_query = new WP_Query( $args );	
									while ($wp_query->have_posts()) : $wp_query->the_post(); 
										?>
											<li id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
												<div class="slide-image"><?php if ( has_post_thumbnail() ) { the_post_thumbnail('full'); } ?></div>
												<div class="project-slide-desc">
													<h3><?php echo get_the_term_list( $post->ID, 'portfolio_categories', ' ', ', ', '' ); ?></h3>
													<h2><?php the_title(); ?></h2>
													<a href="<?php the_permalink(); ?>" class="view-port">
														<?php if(get_option('valise_sportfolio_link')!="") { echo get_option('valise_sportfolio_link'); } else { _e( 'View Portfolio', CSB_THEME_NAME ); } ?>
													</a>
												</div>
											</li>
										<?php
									endwhile;	
								?>
							</ul>	
						</div>	
					</div>
				</section>
			<?php
		}		
	}


	/***************
	SERVICES
	***************/

	function valise_services_list() {
		global $post;

		if(ot_get_option('valise_services_hide')=="") {
			?>
				<!--SERVICES-->
				<section class="home-services">
					<div class="inside services-block clear">
                    <ul>
                     
						<?php
							$services_order = ot_get_option('valise_services_order');
							$services_orderby = ot_get_option('valise_services_orderby');
							$services_limit = ot_get_option('valise_num_hservices');

							$args = array( 'post_type' => 'services', 'orderby' => $services_orderby, 'order' => $services_order, 'posts_per_page' => $services_limit );		
							$wp_query = new WP_Query( $args );	
							for($i = 1; $wp_query->have_posts(); $i++) { 							
								$wp_query->the_post();			
								$columns = 3;	
								$class = 'services-third';
								$class .= ($i % $columns == 3) ? 'column-last' : '';
								?>
                                <div class="column column-1-<?php echo $columns ?> <?php echo $class ?>">
                                        <div id="post-<?php the_ID(); ?>" <?php post_class( array( 'contentblock services-list', $class) ); ?> >
                                            
                                            <div class="iconcircle">
                                                <i class="<?php echo get_post_meta( $post->ID, "services_icon", true ); ?>"></i>
                                            </div>
                                            
                                            <div class="services-desc">
                                                <h5><?php the_title(); ?></h5>
                                                <?php the_excerpt(); ?>
                                                
                                                <?php if(get_post_meta( $post->ID, "services_button_link", true)) { ?><br />
                                                    <a href="<?php echo get_post_meta( $post->ID, "services_button_link", true ); ?>" class="su-button">
                                                        <?php if(get_post_meta( $post->ID, "services_button_text", true )) { echo get_post_meta( $post->ID, "services_button_text", true ); } else { _e( 'Read More', CSB_THEME_NAME ); } ?>
                                                    </a>
                                                <?php } ?>
                                                
                                            </div>
                                        </div>
                                  </div>
								<?php
							};	
						?>
                        
                      </ul>
					</div>
				</section>
			<?php
		}
	}


	/***************
	WELCOME TEXT
	***************/

	function valise_welcome() {
		if(ot_get_option('valise_welcome_hide')=="") {
			?>
				<!--WELCOME-->
				<section class="welcome-text">
					<div class="inside welcome-block clear">
						<?php if(ot_get_option('valise_welcome_header')) { echo ot_get_option('valise_welcome_header'); } else { _e( '<h2>Welcome Text Section</h2>', CSB_THEME_NAME ); } ?>
					</div>
				</section>
			<?php
		}		
	}


	/***************
	BLOG
	***************/

	function valise_blog_list() {
		global $post, $blog_page_url, $class;
		?>
        
        	<?php if(ot_get_option('valise_blog_hide')=="") { ?>
			<!--BLOG-->
			<div class="blog-block">
				<h2><?php if(ot_get_option('valise_blog_hheader')) { echo ot_get_option('valise_blog_hheader'); } else { _e( 'Latest Articles', CSB_THEME_NAME ); } ?></h2>
				<a href="<?php if(ot_get_option('valise_blog_link')) { echo ot_get_option('valise_blog_link'); } else { echo $blog_page_url; } ?>" class="view-all">
					<?php if(ot_get_option('valise_blog_text')) { echo ot_get_option('valise_blog_text'); } else { _e( 'View All', CSB_THEME_NAME ); } ?>
				</a>
				<div class="clear">
					<?php
						$blog_limit = ot_get_option('valise_num_hblog');

						$args = array( 'post_type' => 'post', 'posts_per_page' => $blog_limit );		
						$wp_query = new WP_Query( $args );	
						for($i = 1; $wp_query->have_posts(); $i++) { 							
							$wp_query->the_post();			
							$columns = 3;	
							$class = 'blog-list ';
							$class .= ($i % $columns == 0) ? 'last' : '';

							get_template_part( 'includes/templates/loop', 'grid' );

						};	
					?>
				</div>
			</div>
		<?php
	}
}


	/***************
	TESTIMONIALS
	***************/

	function valise_testimonial_list() {
		global $post;
		?>
        <?php if(ot_get_option('valise_testimonial_hide')=="") { ?>
			<!--TESTIMONIALS-->
			<div class="testimonial-block">
				<div class="testi-flexslider">
					<ul class="slides">
						<?php
							$testimonial_order = ot_get_option('valise_testimonial_order');
							$testimonial_orderby = ot_get_option('valise_testimonial_orderby');
							$testimonial_limit = ot_get_option('valise_num_testimonial');

							$args = array( 'post_type' => 'testimonials', 'orderby' => $testimonial_orderby, 'order' => $testimonial_order, 'posts_per_page' => $testimonial_limit );		
							$wp_query = new WP_Query( $args );	
							while ($wp_query->have_posts()) : $wp_query->the_post(); 
								?>
									<li id="post-<?php the_ID(); ?>" <?php post_class('clear'); ?>>
                                    	<?php if(get_post_meta( $post->ID, "testimonial_client_img", true)) { ?>
                                    	<img src="<?php echo get_post_meta( $post->ID, "testimonial_client_img", true ); ?>" alt="<?php the_title(); ?>" />
                                        <?php  } ?>
											<?php the_content(); ?>
										<div class="testi-details">
											<span><?php the_title(); ?></span>
                                            <?php if(get_post_meta( $post->ID, "testimonial_company", true)) { ?>
                                                <p>&nbsp;<i class="icon-star"></i>&nbsp; <?php echo get_post_meta( $post->ID, "testimonial_company", true ); ?></p>
                                            <?php } ?>
										</div>
									</li>
								<?php						
							endwhile;	
						?>
					</ul>
				</div>
			</div>
		<?php
		}
	}

	?>