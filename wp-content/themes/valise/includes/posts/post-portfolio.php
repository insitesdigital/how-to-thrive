<?php
	/**********************************************
	CUSTOM POST TYPE: PORTFOLIO
	***********************************************/	

	add_action( 'init', 'valise_portfolio_post_type' );
	function valise_portfolio_post_type() { 

		if(ot_get_option('valise_portfolio_cpt')) { $portfolio_name = ot_get_option('valise_portfolio_cpt'); } else { $portfolio_name = "Portfolio"; }
		if(ot_get_option('valise_portfolio_cpt_slug')) { $portfolio_slug = ot_get_option('valise_portfolio_cpt_slug'); } else { $portfolio_slug = "portfolio-item"; }
		if(ot_get_option('valise_portfolio_add')) { $portfolio_add = ot_get_option('valise_portfolio_add'); } else { $portfolio_add = "Add Portfolio"; }
		if(ot_get_option('valise_portfolio_edit')) { $portfolio_edit = ot_get_option('valise_portfolio_edit'); } else { $portfolio_edit = "Edit Portfolio"; }
		
		register_post_type( 'portfolio',
			array(
				'labels' => array(
					'name' => $portfolio_name,
					'singular_name' => $portfolio_name,
					'add_new_item' => $portfolio_add,
					'edit_item' => $portfolio_edit,
				),
				'public' => true,
				'has_archive' => true,	
				'publicly_queryable' => true,
				'show_ui' => true, 
			    'show_in_menu' => true, 
			    'query_var' => true,	
			    'rewrite' => array( 'slug' => $portfolio_slug, 'with_front' => TRUE ),
			    'supports' => array('title', 'editor', 'author', 'thumbnail', 'page-attributes'),
				'menu_icon' => get_template_directory_uri() . '/includes/posts/img/portfolio.png'		
			)
		);
		flush_rewrite_rules();
	}
	
	/*META*/
	function valise_portfolio_meta() {
		global $post;

		$url = get_post_meta($post->ID, 'url', TRUE);
		$exclude = get_post_meta($post->ID, 'exclude', TRUE);
		$hdimage = get_post_meta($post->ID, 'hdimage', TRUE);

		?>
			
            		
		<?php	
	}

	
	/*TAXONOMIES*/
	add_action( 'init', 'valise_portfolio_taxonomies', 0 );
	function valise_portfolio_taxonomies() {
		if(ot_get_option('valise_portfolio_tax_slug')) { $portfolio_tax_slug = ot_get_option('valise_portfolio_tax_slug'); } else { $portfolio_tax_slug = "portfolio-category"; }

		register_taxonomy( 'portfolio_categories', 'portfolio', array( 'hierarchical' => true, 'label' => 'Categories', 'query_var' => true, 'rewrite' => array('slug' => $portfolio_tax_slug) ) );
	}

	/*CUSTOM COLUMNS*/
	add_filter( 'manage_edit-portfolio_columns', 'valise_edit_portfolio_columns' ) ;
	function valise_edit_portfolio_columns( $columns ) {
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'title' => __( 'Name', CSB_THEME_NAME ),						
			'category' => __( 'Categories', CSB_THEME_NAME ),
			'author' => __( 'Author', CSB_THEME_NAME ),
			'thumbnail' => __( 'Thumbnail', CSB_THEME_NAME ),
			'date' => __( 'Date', CSB_THEME_NAME )
		);
		return $columns;
	}

	/*CUSTOM COLUMNS LIST*/
	add_action( 'manage_portfolio_posts_custom_column', 'valise_manage_portfolio_columns', 10, 2 );
	function valise_manage_portfolio_columns( $column, $post_id ) {
		global $post;

		switch($column) {
			case 'category' :
				$terms = get_the_terms( $post_id, 'portfolio_categories' );
				if ( !empty( $terms ) ) {
					$out = array();
					foreach ( $terms as $term ) {
						$out[] = sprintf( '<a href="%s">%s</a>',
							esc_url( add_query_arg( array( 'post_type' => $post->post_type, 'portfolio_categories' => $term->slug ), 'edit.php' ) ),
							esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'portfolio_categories', 'display' ) )
						);
					}
					echo join( ', ', $out );
				}				
				else { __( 'No Categories', CSB_THEME_NAME ); }
			break;

			case 'thumbnail' : 
      			echo the_post_thumbnail( array(35,35) );
      		break;
			
			default :
			break;
		}
	}


	/**********************************************
	SAVE & UPDATE CUSTOM FIELDS
	***********************************************/

	
	add_action('save_post', 'valise_custom_posts_save_portfolio');
	function valise_custom_posts_save_portfolio( $post_id ){
		
		if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || (defined('DOING_AJAX') && DOING_AJAX)) return;
		if ( 'page' == isset($_POST['post_type']) ) { if ( !current_user_can( 'edit_page', $post_id ) ) return;
		} else { if ( !current_user_can( 'edit_post', $post_id ) ) return; }
		
		update_post_meta($post_id, "exclude", @$_POST["exclude"]);
		update_post_meta($post_id, "url", @$_POST["url"]);
		update_post_meta($post_id, "hdimage", @$_POST["hdimage"]);
	}	
?>