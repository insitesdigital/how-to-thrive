<?php
	/**********************************************
	CUSTOM POST TYPE: SERVICES
	***********************************************/	
	
	add_action( 'init', 'valise_services_post_type' );

	function valise_services_post_type() {

		if(ot_get_option('valise_services_cpt')) { $services_name = ot_get_option('valise_services_cpt'); } else { $services_name = "Services"; }
		if(ot_get_option('valise_services_cpt_slug')) { $services_slug = ot_get_option('valise_services_cpt_slug'); } else { $services_slug = "services-item"; }
		if(ot_get_option('valise_services_add')) { $services_add = ot_get_option('valise_services_add'); } else { $services_add = "Add New Services"; }
		if(ot_get_option('valise_services_edit')) { $services_edit = ot_get_option('valise_services_edit'); } else { $services_edit = "Edit Services"; }

		register_post_type( 'services',
			array(
				'labels' => array(
					'name' => $services_name,
					'singular_name' => $services_name,
					'add_new_item' => $services_add,
					'edit_item' => $services_edit,
				),
				'public' => true,
				'has_archive' => true,	
				'exclude_from_search' => true,
				'rewrite' => array('slug' => $services_slug, 'with_front' => TRUE),
				'supports' => array('title','editor', 'author', 'page-attributes', 'thumbnail'),
				'menu_icon' => get_template_directory_uri() . '/includes/posts/img/services.png'					
			)
		);
		flush_rewrite_rules();
	}	


	/*CUSTOM COLUMNS*/
	add_filter( 'manage_edit-services_columns', 'valise_edit_services_columns' ) ;
	function valise_edit_services_columns( $columns ) {
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'title' => __( 'Title', CSB_THEME_NAME ),		
			'author' => __( 'Author', CSB_THEME_NAME ),
			'date' => __( 'Date', CSB_THEME_NAME )
		);
		return $columns;
	}


	/**********************************************
	SAVE & UPDATE CUSTOM FIELDS
	***********************************************/
	
	
	add_action('save_post', 'valise_custom_posts_save_services');
	function valise_custom_posts_save_services( $post_id ){
		
		if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || (defined('DOING_AJAX') && DOING_AJAX)) return;
		if ( 'page' == isset($_POST['post_type']) ) { if ( !current_user_can( 'edit_page', $post_id ) ) return;
		} else { if ( !current_user_can( 'edit_post', $post_id ) ) return; }
		
		update_post_meta($post_id, "hdimage", @$_POST["hdimage"]);
	}	
?>