<?php
	/**********************************************
	CUSTOM POST TYPE: TESTIMONIALS
	***********************************************/
	
	add_action( 'init', 'valise_testimonial_post_type' );
	function valise_testimonial_post_type() {

		if(ot_get_option('valise_testimonial_cpt')) { $testimonial_name = ot_get_option('valise_testimonial_cpt'); } else { $testimonial_name = "Testimonials"; }
		if(ot_get_option('valise_testimonial_cpt_slug')) { $testimonial_slug = ot_get_option('valise_testimonial_cpt_slug'); } else { $testimonial_slug = "testimonial-item"; }
		if(ot_get_option('valise_testimonial_add')) { $testimonial_add = ot_get_option('valise_testimonial_add'); } else { $testimonial_add = "Add New Testimonial"; }
		if(ot_get_option('valise_testimonial_edit')) { $testimonial_edit = ot_get_option('valise_testimonial_edit'); } else { $testimonial_edit = "Edit Testimonial"; }

		register_post_type( 'testimonials',
			array(
				'labels' => array(
					'name' => $testimonial_name,
					'singular_name' => $testimonial_name,
					'add_new_item' => $testimonial_add,
					'edit_item' => $testimonial_edit,
				),
				'public' => true,
				'has_archive' => true,
				'rewrite' => array( 'slug' => $testimonial_slug, 'with_front' => TRUE ),
				'supports' => array('title','editor','author'),
				'menu_icon' => get_template_directory_uri() . '/includes/posts/img/testimonials.png'				
			)
		);
		flush_rewrite_rules();
	}

	/*CUSTOM COLUMNS*/
	add_filter( 'manage_edit-testimonial_columns', 'edit_testimonial_columns' ) ;
	function edit_testimonial_columns( $columns ) {
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'title' => __( 'Name', CSB_THEME_NAME ),			
			'author' => __( 'Author', CSB_THEME_NAME ),
			'date' => __( 'Date', CSB_THEME_NAME )
		);
		return $columns;
	}

	/**********************************************
	SAVE & UPDATE CUSTOM FIELDS
	***********************************************/
	
	add_action('save_post', 'valise_custom_posts_save_testimonial');
	function valise_custom_posts_save_testimonial( $post_id ){
		
		if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || (defined('DOING_AJAX') && DOING_AJAX)) return;
		if ( 'page' == isset($_POST['post_type']) ) { if ( !current_user_can( 'edit_page', $post_id ) ) return;
		} else { if ( !current_user_can( 'edit_post', $post_id ) ) return; }
	}	
?>