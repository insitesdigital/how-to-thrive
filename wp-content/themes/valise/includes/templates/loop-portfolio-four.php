<?php global $class; ?>

<div id="post-<?php the_ID(); ?>" <?php post_class($class); ?>>
    
    <div class="port-hover">
        <?php 
			if ( post_password_required() ) {
				?>
				<div class="password-protect-thumb port-2cols"><h3><?php _e( 'Password Protected', CSB_THEME_NAME ); ?></h3></div>
				<?php
			}else {
				if ( has_post_thumbnail() ) { the_post_thumbnail(); } 
			}
			$thumb_id = get_post_thumbnail_id();
			$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
		?>
        <div class="overlay">
            <a href="<?php the_permalink(); ?>" class="details"><i class="icon-share-alt"></i>Details</a>
            <a href="<?php echo $thumb_url[0]; ?>" class="zoom fancybox"><i class="icon-eye-open"></i>View</a>
        </div>
    </div>
    
    
	<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    <?php if(get_post_meta( $post->ID, "pro_short_title", true)) { ?>
    <p><?php echo get_post_meta( $post->ID, "pro_short_title", true ); ?></p>
    <?php } else { echo ''; } ?>
</div>