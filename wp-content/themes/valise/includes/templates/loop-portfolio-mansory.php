<?php global $class;
	
	$thumb_size = get_post_meta($post->ID, 'thumb_size', true);
	?>

<div class="mansory-wrapper">
<div id="post-<?php the_ID(); ?>" <?php post_class( array( 'mansory', $class, $thumb_size ) ); ?>>
    
    <div class="port-hover <?php echo $thumb_size ?>">
        <?php 
			if ( post_password_required() ) {
				?>
				<div class="password-protect-thumb port-2cols"><h3><?php _e( 'Password Protected', CSB_THEME_NAME ); ?></h3></div>
				<?php
			}
			$thumb_id = get_post_thumbnail_id();
			$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
		?>
        
        <?php if (get_post_meta($post->ID,'thumb_size', true) == 'thumb_large') {
						if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
						  the_post_thumbnail('cs-size-large');
						}
				}
			?>
            
            <?php if (get_post_meta($post->ID,'thumb_size', true) == 'thumb_medium') {
						if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
						  the_post_thumbnail('cs-size-medium');
						}
				}
			?>
            
            <?php if (get_post_meta($post->ID,'thumb_size', true) == 'thumb_verticle') {
						if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
						  the_post_thumbnail('cs-size-verticle');
						}
				}
			?>
            
            <?php if (get_post_meta($post->ID,'thumb_size', true) == 'thumb_horizontal') {
						if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
						  the_post_thumbnail('cs-size-horizontal');
						}
				}
			?>
        
        
        <div class="overlay">
            <a href="<?php the_permalink(); ?>" class="details"><i class="icon-share-alt"></i>Details</a>
            <a href="<?php echo $thumb_url[0]; ?>" class="zoom fancybox"><i class="icon-eye-open"></i>View</a>
        </div>
    </div>
    
    
	<h5>
		<?php the_title(); ?>

	</h5>
</div>
</div>