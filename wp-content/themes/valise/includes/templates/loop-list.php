<div id="post-<?php the_ID(); ?>" <?php post_class('blog-list-sidebar'); ?>>
	<div class="bimage">
    
    <div class="blogdetails">
      <div class="date">
        <span class="number"><?php echo get_the_date('j'); ?></span><?php echo get_the_date('M'); ?>
      </div>
    </div>    
        
        <?php if ( ( function_exists( 'get_post_format' ) && 'gallery' == get_post_format( $post->ID ) )  ) { ?>
        
        	<div class="blog-flexslider gallery-post">
                <ul class="slides">
                    <?php
                        $args = array( 'post_type' => 'attachment', 'orderby' => 'menu_order', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $post->ID );

                        $attachments = get_posts( $args );
                        if ( $attachments ) {								
                            foreach ( $attachments as $attachment ) {
                                $attachment_id = $attachment->ID;
                                $type = get_post_mime_type( $attachment->ID ); 

                                switch ( $type ) {
                                    case 'video/mp4':
                                        ?>
                                        <li>
                                            <video id="videojs_gallery" class="video-js vjs-default-skin" controls preload="none" width="364" height="100%" poster="" data-setup="{}">
                                                <source src="<?php echo wp_get_attachment_url( $attachment->ID ); ?>" type='video/mp4' />
                                            </video>
                                        </li>
                                        <?php
                                    break;
                                    default:
                                        ?>
                                        <li><?php echo wp_get_attachment_image( $attachment->ID, 'blog-thumb' ); ?></li>
                                        <?php
                                    break;
                                }
                            }
                        }			
                    ?>
                </ul>
            </div>
       
       <?php } elseif ( ( function_exists( 'get_post_format' ) && 'video' == get_post_format( $post->ID ) )  ) { if(get_post_meta( $post->ID, "post_video_type", true ) == "self") {
													?>
													<video id="videojs_blog" class="video-js vjs-default-skin" controls preload="none" width="100%" height="300" poster="<?php echo $large_image_url[0]; ?>" data-setup="{}">
														<source src="<?php echo get_post_meta( $post->ID, "post_video_url", true ); ?>" type='video/mp4' />
													    <source src="<?php echo get_post_meta( $post->ID, "post_video_url", true ); ?>" type='video/webm' />
													    <source src="<?php echo get_post_meta( $post->ID, "post_video_url", true ); ?>" type='video/ogg' />
													</video>													
													<?php
												} if(get_post_meta( $post->ID, "post_video_type", true ) == "youtube") {
													?>
													<iframe width="364" height="210" src="http://www.youtube.com/embed/<?php echo get_post_meta( $post->ID, "post_video_url", true ); ?>?autohide=1&amp;iv_load_policy=3&amp;modestbranding=1&amp;rel=0&amp;showinfo=0" allowfullscreen></iframe>													
													<?php
												} if(get_post_meta( $post->ID, "post_video_type", true ) == "vimeo") {
													?>
													<iframe src="http://player.vimeo.com/video/<?php echo get_post_meta( $post->ID, "post_video_url", true ); ?>?title=0&amp;byline=0" width="364" height="210" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>							
													<?php
												} if(get_post_meta( $post->ID, "post_video_type", true ) == "dailymotion") {
													?>
                                                    <iframe width="364" height="210" src="http://www.dailymotion.com/embed/video/<?php echo get_post_meta( $post->ID, "post_video_url", true ); ?>"></iframe>				
													<?php
												}  } else { ?>  
                 
        <div class="lt-img">                               
            
            <div class="port-hover">
				<?php 
                if ( post_password_required() ) {
                    ?>
                    <div class="password-protect-thumb port-2cols"><h3><?php _e( 'Password Protected', CSB_THEME_NAME ); ?></h3></div>
                    <?php
                }else {
					$default_img = ot_get_option('valise_featured_img');
					if ( has_post_thumbnail() ) { { the_post_thumbnail('blog-thumb'); } 
					} else { if(ot_get_option('valise_featured_img')!= "") { echo '<img src="' .$default_img. '" alt="no featured imgage" />'; } else {} }
                }
                $thumb_id = get_post_thumbnail_id();
                $thumb_url = wp_get_attachment_image_src($thumb_id,'blog-fullwidth', true);
                ?>
                <div class="overlay">
                
                <?php
                    if ( ( function_exists( 'get_post_format' ) && 'image' == get_post_format( $post->ID ) )  ) {
                        ?><a href="<?php the_permalink(); ?>" class="details"><i class="icon-share-alt"></i>Details</a>
                <a href="<?php echo $thumb_url[0]; ?>" class="zoom fancybox"><i class="icon-eye-open"></i>View</a><?php		
                    } elseif ( ( function_exists( 'get_post_format' ) && 'video' == get_post_format( $post->ID ) )  ) { 
                        ?><a href="<?php the_permalink(); ?>"><i class="icon-facetime-video icon-large"></i></a><?php	
                    } elseif ( ( function_exists( 'get_post_format' ) && 'gallery' == get_post_format( $post->ID ) )  ) { 
                        ?><a href="<?php the_permalink(); ?>"><i class="icon-th icon-large"></i></a><?php	
                    } elseif ( ( function_exists( 'get_post_format' ) && '' == get_post_format( $post->ID ) )  ) { 
                        ?><a href="<?php the_permalink(); ?>" class="details"><i class="icon-share-alt"></i>Details</a>
            <a href="<?php echo $thumb_url[0]; ?>" class="zoom fancybox"><i class="icon-eye-open"></i>View</a><?php	
                    }
                ?>
                </div>
             </div>
    
        </div>
        
        <?php } ?>
        
		
	</div>
	<div class="blog-details">
		<h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
        
        <label class="clear blog-detail blog-detail-full" style="float: none;">		
            <?php if ( function_exists( 'bawpvc_views_sc' ) ) { ?>
            <span class="featured-blog-details"><i class="icon-eye-open"></i><?php echo bawpvc_views_sc( $content = null ); ?>	</span>
            <?php } else {} ?>
            <span class="featured-blog-details"><i class="icon-tag"></i><?php the_category(', '); ?> </span>
            <span class="featured-blog-details"><i class="icon-comment"></i><?php comments_number( 'No Comments', 'One Comment', '% Comments' ); ?> </span>
        </label>
        
		<?php 
			if(ot_get_option('valise_hide_excerpt')=="") {
				the_excerpt(); 
			}			
		?>
        <a href="<?php the_permalink(); ?>" class="su-button"><?php _e( 'Read More', CSB_THEME_NAME ); ?></a>
	</div>
</div>