<?php
	/**********************************************
	CUSTOM CONTACT INFO WIDGET
	***********************************************/
	
	class valise_Widget_Contact extends WP_Widget {
	
		function valise_Widget_Contact() {			
			$widget_ops = array('classname' => 'widget-contact-info', 'description' => __('Custom widget for contact information', CSB_THEME_NAME));	
			$this -> WP_Widget('Contact', __('Valise: Contact Information', CSB_THEME_NAME), $widget_ops);		
		}

		function widget($args, $instance) {		
			extract($args);		
			$title = apply_filters('widget_title', $instance['title']);		
			if (empty($title)) $title = false;
				$instance_contact_text = array();
				$instance_contact_address = array();
				$instance_contact_phone = array();
				$instance_contact_email = array();				

				$contact_text = 'contact_text';
				$instance_contact_text = isset($instance[$contact_text]) ? $instance[$contact_text] : '';
				$contact_address = 'contact_address';
				$instance_contact_address = isset($instance[$contact_address]) ? $instance[$contact_address] : '';
				$contact_phone = 'contact_phone';
				$instance_contact_phone = isset($instance[$contact_phone]) ? $instance[$contact_phone] : '';
				$contact_email = 'contact_email';
				$instance_contact_email = isset($instance[$contact_email]) ? $instance[$contact_email] : '';				

				echo $before_widget;					
				if ($title) {						
					echo $before_title;
					echo $title;
					echo $after_title;						
				}
				
				?>

				<!--CONTACT INFO-->
				<ul class="clear">
					<li><label><?php echo $instance_contact_text; ?></label></li>
					<li><label><?php echo $instance_contact_address; ?></label></li>
					<li><span>Tel:</span><?php echo "+" . $instance_contact_phone; ?></li>
					<li><span>Email:</span><a href="mailto:<?php echo $instance_contact_email; ?>"><?php echo $instance_contact_email; ?></a></li>
				</ul>

				<?php
					echo $after_widget.'';				
				}
			
				function update($new_instance, $old_instance) {				
					$instance = $old_instance;				
					$instance['title'] = strip_tags($new_instance['title']);
					$instance['contact_text'] = $new_instance['contact_text'];		
					$instance['contact_address'] = $new_instance['contact_address'];		
					$instance['contact_phone'] = $new_instance['contact_phone'];
					$instance['contact_email'] = $new_instance['contact_email'];									
					return $instance;			
				}
			
				function form($instance) {				
					$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
					$instance_contact_text = array();
					$instance_contact_address = array();
					$instance_contact_phone = array();
					$instance_contact_email = array();					

					$contact_text = 'contact_text';
					$instance_contact_text = isset($instance[$contact_text]) ? $instance[$contact_text] : '';
					$contact_address = 'contact_address';
					$instance_contact_address = isset($instance[$contact_address]) ? $instance[$contact_address] : '';		
					$contact_phone = 'contact_phone';
					$instance_contact_phone = isset($instance[$contact_phone]) ? $instance[$contact_phone] : '';
					$contact_email = 'contact_email';
					$instance_contact_email = isset($instance[$contact_email]) ? $instance[$contact_email] : '';
				?>
					<p><label for="<?php echo $this -> get_field_id('title'); ?>"><?php _e('Title:', CSB_THEME_NAME); ?></label>
					<input class="widefat" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
					<div>			
						<div>							
							<p><label for="<?php echo $this -> get_field_id($contact_text); ?>"><?php _e( 'Text:', CSB_THEME_NAME ); ?></label>
							<textarea class="widefat" type="text" id="<?php echo $this -> get_field_id($contact_text); ?>" name="<?php echo $this -> get_field_name($contact_text); ?>"><?php echo $instance_contact_text; ?></textarea>
							</p>	
							<p><label for="<?php echo $this -> get_field_id($contact_address); ?>"><?php _e( 'Address:', CSB_THEME_NAME ); ?></label>
							<textarea class="widefat" type="text" id="<?php echo $this -> get_field_id($contact_address); ?>" name="<?php echo $this -> get_field_name($contact_address); ?>"><?php echo $instance_contact_address; ?></textarea>
							</p>	
							<p><label for="<?php echo $this -> get_field_id($contact_phone); ?>"><?php _e( 'Phone:', CSB_THEME_NAME ); ?></label>
							<input class="widefat" type="text" id="<?php echo $this -> get_field_id($contact_phone); ?>" name="<?php echo $this -> get_field_name($contact_phone); ?>" value="<?php echo $instance_contact_phone; ?>">		
							</p>
							<p><label for="<?php echo $this -> get_field_id($contact_email); ?>"><?php _e( 'Email:', CSB_THEME_NAME ); ?></label>
							<input class="widefat" type="text" id="<?php echo $this -> get_field_id($contact_email); ?>" name="<?php echo $this -> get_field_name($contact_email); ?>" value="<?php echo $instance_contact_email; ?>">		
							</p>											
						</div>			
					</div>
		<?php
				}			
		}

		function valise_widgets_contact() {			
			register_widget('valise_Widget_Contact');			
		}
		add_action('widgets_init', 'valise_widgets_contact');
?>