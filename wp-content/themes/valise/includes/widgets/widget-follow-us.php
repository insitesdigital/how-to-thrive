<?php
	/**********************************************
	CUSTOM FOLLOW US WIDGET
	***********************************************/
	
	class valise_Widget_GetTouch extends WP_Widget {
	
		function valise_Widget_GetTouch() {		
			$widget_ops = array('classname' => 'widget-get-in-touch', 'description' => __('Custom widget for social media icons', CSB_THEME_NAME));	
			$this -> WP_Widget('GetTouch', __('Valise: Social Media', CSB_THEME_NAME), $widget_ops);		
		}
	
		function widget($args, $instance) {		
			extract($args);		
			$title = apply_filters('widget_title', $instance['title']);		
			if (empty($title)) $title = false;
					
				$instance_follow_bdesc = array();

				echo $before_widget;	
				if ($title) {						
					echo $before_title;
					echo $title;
					echo $after_title;						
				}				

				?>	

				<!--FOLLOW US-->
				<ul class="clear follow-us">
					<?php
						if(ot_get_option('valise_facebook')) {
							?>
							<li><a href="<?php echo ot_get_option('valise_facebook'); ?>" title="Facebook" target="_blank" class="facebook"><i class="icon-facebook icon-large"></i></a></li>
							<?php
						}
						if(ot_get_option('valise_twitter')) {
							?>
							<li><a href="<?php echo ot_get_option('valise_twitter'); ?>" title="Twitter" target="_blank" class="twitter"><i class="icon-twitter icon-large"></i></a></li>
							<?php
						}	
						if(ot_get_option('valise_dribbble')) {
							?>
							<li><a href="<?php echo ot_get_option('valise_dribbble'); ?>" title="Dribbble" target="_blank" class="dribbble"><i class="icon-dribbble icon-large"></i></a></li>
							<?php
						}
						if(ot_get_option('valise_linkedin')) {
							?>
							<li><a href="<?php echo ot_get_option('valise_linkedin'); ?>" title="LinkedIn" target="_blank" class="linkedin"><i class="icon-linkedin icon-large"></i></a></li>
							<?php
						}
						
						if(ot_get_option('valise_google')) {
							?>
							<li><a href="<?php echo ot_get_option('valise_google'); ?>" title="Google Plus" target="_blank" class="google-plus"><i class="icon-google-plus icon-large"></i></a></li>
							<?php
						}
						
						if(ot_get_option('valise_instagram')) {
							?>
							<li><a href="<?php echo ot_get_option('valise_instagram'); ?>" title="Instagram" target="_blank" class="instagram"><i class="icon-instagram icon-large"></i></a></li>
							<?php
						}
						
						if(ot_get_option('valise_skype')) {
							?>
							<li><a href="<?php echo ot_get_option('valise_skype'); ?>" title="Skype" target="_blank" class="skype"><i class="icon-skype icon-large"></i></a></li>
							<?php
						}
						
						if(ot_get_option('valise_youtube')) {
							?>
							<li><a href="<?php echo ot_get_option('valise_youtube'); ?>" title="Youtube" target="_blank" class="youtube"><i class="icon-youtube icon-large"></i></a></li>
							<?php
						}
																							
					?>
				</ul>

				<?php
					echo $after_widget.'';				
				}
			
				function update($new_instance, $old_instance) {				
					$instance = $old_instance;				
					$instance['title'] = strip_tags($new_instance['title']);
					return $instance;				
				}
			
				function form($instance) {				
					$title = isset($instance['title']) ? esc_attr($instance['title']) : '';												
					
				?>
					<p><label for="<?php echo $this -> get_field_id('title'); ?>"><?php _e('Title:', CSB_THEME_NAME); ?></label>
					<input class="widefat" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
					<div>	
						<?php _e( 'This Social links are taken from Appearance > Theme Options > Social Media', CSB_THEME_NAME ); ?>
					</div>
		<?php
				}			
		}

		function valise_widgets_gettouch() {			
			register_widget('valise_Widget_GetTouch');			
		}
		add_action('widgets_init', 'valise_widgets_gettouch');
?>