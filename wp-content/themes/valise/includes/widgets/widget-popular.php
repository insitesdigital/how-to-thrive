<?php
	/**********************************************
	CUSTOM POPULAR POSTS
	***********************************************/
	
	class valise_Widget_Popular_Posts extends WP_Widget {
	
		function valise_Widget_Popular_Posts() {		
			$widget_ops = array('classname' => 'widget-popular-posts', 'description' => __('Custom widget for popular posts', CSB_THEME_NAME));	
			$this -> WP_Widget('Popular', __('Valise: Popular Posts', CSB_THEME_NAME), $widget_ops);		
		}
	
		function widget($args, $instance) {		
			extract($args);		
			$title = apply_filters('widget_title', $instance['title']);		
			if (empty($title)) $title = false;
				$instance_posts_limit = array();
				$instance_portfolio_bdesc = array();
				
				$posts_limit = 'posts_limit';
				$instance_posts_limit = isset($instance[$posts_limit]) ? $instance[$posts_limit] : '';

				echo $before_widget;					

				if ($title) {						
					echo $before_title;
					echo $title;
					echo $after_title;						
				}
				?>	
					
				<!--POPULAR POSTS-->
				<ul>
					<?php
						$args = array( 'post_type' => 'post', 'orderby' => 'comment_count', 'posts_per_page' => $instance_posts_limit );
						$posts_loop = new WP_Query( $args );	
						while ($posts_loop->have_posts()) : $posts_loop->the_post();	
					?>
						<li>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>									
						</li>	
					<?php
						endwhile;	
						wp_reset_query();	
					?>		
				</ul>		

				<?php
					echo $after_widget.'';				
				}
			
				function update($new_instance, $old_instance) {				
					$instance = $old_instance;				
					$instance['title'] = strip_tags($new_instance['title']);
					$instance['posts_limit'] = $new_instance['posts_limit'];				
					return $instance;				
				}
			
				function form($instance) {				
					$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
					$instance_posts_limit = array();
					
					$posts_limit = 'posts_limit';
					$instance_posts_limit = isset($instance[$posts_limit]) ? $instance[$posts_limit] : '';			
					
				?>
					<p><label for="<?php echo $this -> get_field_id('title'); ?>"><?php _e('Title:', CSB_THEME_NAME); ?></label>
					<input class="widefat" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>					
					<div>			
						<div>
							<p><label for="<?php echo $this -> get_field_id($posts_limit); ?>"><?php _e('Limit:', CSB_THEME_NAME); ?></label>
							<input class="widefat" type="text" id="<?php echo $this -> get_field_id($posts_limit); ?>" name="<?php echo $this -> get_field_name($posts_limit); ?>" value="<?php echo $instance_posts_limit; ?>">		
							</p>											
						</div>	
						<p><small><i><?php _e( 'Popular Posts are automatically displayed from blog posts', CSB_THEME_NAME ); ?></i></small></p>		
					</div>
		<?php
				}			
		}

		function valise_widgets_popular_posts() {			
			register_widget('valise_Widget_Popular_Posts');			
		}
		add_action('widgets_init', 'valise_widgets_popular_posts');
?>