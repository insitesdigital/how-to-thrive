<?php
/**********************************************
	Hire Me Widget
***********************************************/

class hire_me extends WP_Widget {

	function __construct()
	{
		$params = array(
			'description' => 'Draw and put your details',
			'name'		  => 'Valise: Hire Me'
		);
		
		parent::__construct('hire_me', '', $params);
		
	}
		
	public function form($instance)
	{
		extract($instance);
?>

			<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title :</label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php if( isset($title) ) echo esc_attr($title); ?>" />
			</p>
			
			<hr style="background: #ccc; border: 0; height: 1px; margin: 5px 0 10px;" />

			<h1 align="center" style="font-size:20px;">Hire Me</h1>
			
			<hr style="background: #ccc; border: 0; height: 1px; margin: 5px 0 10px;" />
			
			<p>
			<label for="<?php echo $this->get_field_id('personal_name'); ?>">Name :</label>
			<input class="widefat" type="text" id="<?php echo $this->get_field_id('personal_name'); ?>" name="<?php echo $this->get_field_name('personal_name'); ?>" value="<?php if( isset($personal_name) ) echo esc_attr($personal_name); ?>" placeholder="Your Name..."  cols="15" rows="30" ></input>
			</p>
			
			<p>
			<label for="<?php echo $this->get_field_id('personal_img'); ?>">Your Image(140x140) :</label>
			<input class="widefat" type="text" id="<?php echo $this->get_field_id('personal_img'); ?>" name="<?php echo $this->get_field_name('personal_img'); ?>" value="<?php if( isset($personal_img) ) echo esc_attr($personal_img); ?>" placeholder="Your Image URL.."  cols="15" rows="30" ></input>
			</p>
			
			<p>
			<label for="<?php echo $this->get_field_id('personal_skill_location'); ?>">Skill &amp; Location :</label>
			<input class="widefat" type="text" id="<?php echo $this->get_field_id('personal_skill_location'); ?>" name="<?php echo $this->get_field_name('personal_skill_location'); ?>" value="<?php if( isset($personal_skill_location) ) echo esc_attr($personal_skill_location); ?>" placeholder="UX/UI Designer, London" />
			</p>
			
			<p>
			<label for="<?php echo $this->get_field_id('personal_info'); ?>">Short Info :</label>
			<input class="widefat" type="text" id="<?php echo $this->get_field_id('personal_info'); ?>" name="<?php echo $this->get_field_name('personal_info'); ?>" value="<?php if( isset($personal_info) ) echo esc_attr($personal_info); ?>" placeholder="(Max : 65 Characters)" />
			
			</p>
			
			<p>
			<label for="<?php echo $this->get_field_id('button_text'); ?>">Hire Me Button Text :</label>
			<input class="widefat" type="text" id="<?php echo $this->get_field_id('button_text'); ?>" name="<?php echo $this->get_field_name('button_text'); ?>" value="<?php if( isset($button_text) ) echo esc_attr($button_text); ?>" placeholder="Default : Hire Me"  cols="15" rows="30" ></input>
			</p>
            
            <p>
			<label for="<?php echo $this->get_field_id('button_link'); ?>">Hire Me Button Link :</label>
			<input class="widefat" type="text" id="<?php echo $this->get_field_id('button_link'); ?>" name="<?php echo $this->get_field_name('button_link'); ?>" value="<?php if( isset($button_link) ) echo esc_attr($button_link); ?>" placeholder="Your hire me button link..."  cols="15" rows="30" ></input>
			</p>
			
			<hr style="background: #ccc; border: 0; height: 1px; margin: 5px 0 10px;" />
			</p>
			
			
					
		<?php
	}

	public function widget($args, $instance)
	{
	
		extract($args);
		extract($instance);
		
		if ( empty($title) ) $title = '';
		
		echo $before_widget;
			
			echo '<div class="hr-author">';
			
			echo '<h2>'. $title .'</h2>';
			
				echo '<div class="personal_details">';	
				
				echo '<img src="'. $personal_img .'" alt="' . $personal_name .'">';
				
					echo '<div class="short_details">';
					
					echo '<h3>'. $personal_name .'</h3>';
					
					echo '<h4>'. $personal_skill_location .'</h4>';
					
					echo '<p>'. $personal_info .'</p>';

				echo '</div>';
				
				echo '</div>';
				
				echo '<div class="hr-button">';
				if ( $button_link ) {
					if ( $button_text ) {
					echo '<a href="'. $button_link .'" class="su-button"><i class="icon-heart-empty"></i> '. $button_text .'</a>';
					} else {
						echo '<a href="'. $button_link .'" class="su-button"><i class="icon-heart-empty"></i> Hire Me</a>';
					}
				}
				
				echo '</div>';

			echo '</div>';
			
		echo $after_widget;			
	
	} 
	
}

add_action('widgets_init', 'hire_me');

function hire_me()
{
	register_widget('hire_me');
}

?>