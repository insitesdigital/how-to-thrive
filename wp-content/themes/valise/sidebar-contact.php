<?php
	if ( is_active_sidebar( 'contact_sidebar' ) ) :
		dynamic_sidebar( 'contact_sidebar' );
	else :
		_e( 'This is a widget area, so please add widgets here...', CSB_THEME_NAME );
	endif;
?>