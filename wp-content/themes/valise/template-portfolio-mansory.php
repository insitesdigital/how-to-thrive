<?php
/*
Template Name: Portfolio - Mansory
*/
?>

<?php get_header(get_post_meta($post->ID, 'header_choice_select', true)); ?>
    
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "revolution-slider") { 
		}
	?>
    
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "notitle") { 
		echo '<hr>';
		}
	?>
    
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "") { 
	?>
        <!--BREADCRUMB / TITLE-->
        <section id="title-wrapper" class="title-breadcrumb">
            <div class="inside">
                <?php 
                    valise_get_page_title(); 
                    valise_breadcrumb(); 
                ?>
            </div>
        </section>
	<?php
		}
	?>

	<!--PORTFOLIO-->
	<section class="project-page">
		<div class="inside clear">

			<div class="masonry clear">
				<?php
					valise_get_home_pagination();
					$portfolio_order = get_option('valise_album_order');
					$portfolio_orderby = get_option('valise_album_orderby');
					$portfolio_limit = ('30');

					$args = array( 'post_type' => 'portfolio', 'orderby' => $portfolio_orderby, 'order' => $portfolio_order, 'posts_per_page' => $portfolio_limit, 'paged' => $paged, 'meta_query' => array( array( 'key' => 'exclude', 'value' => 'on', 'compare' => '!=' ) ) );		
					$wp_query = new WP_Query( $args );	
					for($i = 1; $wp_query->have_posts(); $i++) { 							
						$wp_query->the_post();			
						$columns = 2;	
						$class = 'project-list two-cols ';
						$class .= ($i % $columns == 0) ? 'last' : '';

						get_template_part( 'includes/templates/loop', 'portfolio-mansory' );

					};						
				?>
			</div>
			<?php
				if(ot_get_option('valise_portfolio_pnav')=="Next Previous Link") { valise_next_previous(); }else { valise_pagination(); }
			?>

		</div>
        
	</section>

<?php get_footer(); ?>