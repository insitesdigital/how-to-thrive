<?php get_header(); ?>

        <!--BREADCRUMB / TITLE-->
        <section id="title-wrapper" class="title-breadcrumb">
            <div class="inside">
                <h2 class="ptitle"><?php if(ot_get_option('valise_search_header')!= "") { echo ot_get_option('valise_search_header'); } else { _e( 'Search Results', CSB_THEME_NAME ); } ?></h2>
                <?php 
                    valise_breadcrumb(); 
                ?>
            </div>
        </section>

	<!--BLOG SIDEBAR-->
	<section class="blog-page">
		<div class="inside clear">

			<!--LEFT CONTAINER-->
			<div class="right-content <?php if(ot_get_option('valise_blog_sidebar')=="Right") { echo "left"; } ?>">
				<div class="masonry clear">
					<?php
						for($i = 1; have_posts(); $i++) { 							
							the_post();			
							$columns = 2;	
							$class = 'blog-list ';
							$class .= ($i % $columns == 0) ? 'last' : '';
							
							get_template_part( 'includes/templates/loop', 'grid' );

						};						
					?>
				</div>
				<?php
					if(ot_get_option('valise_blog_pnav')=="Next Previous Link") { valise_next_previous(); }else { valise_pagination(); }
				?>
			</div>

			<!--RIGHT SIDEBAR-->
			<div class="left-sidebar <?php if(ot_get_option('valise_blog_sidebar')=="Right") { echo "right"; } ?>">
				<?php
					if($sidebar_choice == "Default") { get_sidebar( 'blog' ); } else { get_sidebar( 'blog' ); }
				?>
			</div>

		</div>
	</section>

<?php get_footer(); ?>