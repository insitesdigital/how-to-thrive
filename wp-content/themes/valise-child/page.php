<?php get_header(get_post_meta($post->ID, 'header_choice_select', true)); ?>

	
    
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "revolution-slider") { 
		}
	?>
    
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "notitle") { 
		echo '<hr>';
		}
	?>
    
    <?php if( get_post_meta( $post->ID, "header_choice_select", true ) == "") { 
	?>
        <!--BREADCRUMB / TITLE-->
        <section id="title-wrapper" class="title-breadcrumb">
            <div class="inside">
                    <?php
                    	if( get_post_meta( $post->ID, "pages_title", true ) != "") { 
							?><h2 class="ptitle"><?php echo stripslashes(get_post_meta( $post->ID, "pages_title", true )); ?></h2><?php 
						}else {
							?><h2 class="ptitle"><?php the_title(); ?></h2><?php
						}
                    ?> 
                <?php 
                    valise_breadcrumb(); 
                ?>
            </div>
        </section>
	<?php
		}
	?>
    

	<section class="theme-pages">
		<div class="inside clear">

			<!--LEFT CONTAINER-->
			<div class="right-content left">
				<?php
					if (have_posts()) : 
						while (have_posts()) : the_post(); 
							the_content(); 								
						endwhile; 
					endif;
				?>
                
                <?php if(ot_get_option('valise_page_comments')) { comments_template(); } ?>
			</div>
            
			<!--RIGHT SIDEBAR-->
			<div class="left-sidebar right">
				<?php
					if($sidebar_choice == "Default") { get_sidebar(); } else { valise_get_custom_sidebar(); }
				?>
			</div>

		</div>
	</section>

<?php get_footer(); ?>