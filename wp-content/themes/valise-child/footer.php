<footer>	
	<?php
		if(!ot_get_option('valise_footer_widget_hide')) {
			?>
			<!--FOOTER WIDGETS-->
			<div class="footer-widgets clear">
				<div class="inside">
					<div class="widget-column">
						<?php
							if ( is_active_sidebar( 'footer_one' ) ) : dynamic_sidebar( 'footer_one' );
							else : _e( 'This is a widget area, so please add widgets here...', CSB_THEME_NAME );
							endif;
						?>
					</div>
					<div class="widget-column">
						<?php
							if ( is_active_sidebar( 'footer_two' ) ) : dynamic_sidebar( 'footer_two' );
							else : _e( 'This is a widget area, so please add widgets here...', CSB_THEME_NAME );
							endif;
						?>
					</div>
					<div class="widget-column">
						<?php
							if ( is_active_sidebar( 'footer_three' ) ) : dynamic_sidebar( 'footer_three' );
							else : _e( 'This is a widget area, so please add widgets here...', CSB_THEME_NAME );
							endif;
						?>
					</div>
					<div class="widget-column last">
						<?php
							if ( is_active_sidebar( 'footer_four' ) ) : dynamic_sidebar( 'footer_four' );
							else : _e( 'This is a widget area, so please add widgets here...', CSB_THEME_NAME );
							endif;
						?>
					</div>
				</div>
			</div>
			<?php						
		}
	?>

	<div class="footer-main">
		<div class="inside clear">
			<label class="copyright">
				<?php
					if(ot_get_option('copyright_area_text')) {
						echo ot_get_option('copyright_area_text');
					}else {
						_e( 'Copyright &copy; ', CSB_THEME_NAME ); echo date('Y'); echo "&nbsp;&dash;&nbsp;"; bloginfo('name'); echo ".&nbsp;"; _e( 'All Rights Reserved.', CSB_THEME_NAME );			
					}
				?>
			</label>
			<?php wp_nav_menu( array( 'theme_location' => 'secondary-menu', 'container_class' => 'footer-menu clear' ) ); ?>
		</div>	
	</div>			
</footer>	

<a href="#" id="toTop"><i class="icon-chevron-up"></i></a>	

<?php wp_footer(); ?>
</div>
</body>
</html>